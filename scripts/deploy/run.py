import sys

import fabric
import argparse
import os
import logging
from patchwork import files

from fabric import Connection, Config

logger = logging.getLogger()

VIRTUALENVWRAPPER = "/usr/share/virtualenvwrapper/virtualenvwrapper.sh"

BOARDLS_PRODRC = """
export ENVIRONMENT=production
export FLASK_APP=autoapp.py
export INTERFACE_SERVER_API_URL='http://10.30.0.102:5000/instances/1/'
#export INSTRUMENTS_SERVER_API_URL='http://10.30.0.102:5000/instances/1/'
export INSTRUMENTS_SERVER_API_URL='http://127.0.0.1:5000/instances/1/'

export SECRET_KEY='sdaasfddfasfdasdfasdfasdfasdfsadf'
export FLASK_CONFIG='production'
export SCRIPT_NAME='/labs/{type}'
export SESSION_COOKIE_PATH='/labs/{type}'

export CAMERA_URL='https://weblab.psut.edu.jo/cams/cams/{type}'
export WEBLAB_USERNAME=weblabdeusto
export WEBLAB_PASSWORD=set-correct-weblab-password-here

export REDIS_BASE={type}
export BOARD_TYPE={type}

export INSTITUTION={institution}

. /home/elives/.virtualenvs/board-ls/bin/activate
"""


WEBLAB_CONFIG_YML_APPEND = """
          rlc:
            type: experiment
            class: experiments.http_experiment.HttpExperiment
            config:
              http_experiment_url: {url}/labs/rlc
              http_experiment_username: weblabdeusto
              http_experiment_password: {password}
          opamp:
            type: experiment
            class: experiments.http_experiment.HttpExperiment
            config:
              http_experiment_url: {url}/labs/opamp
              http_experiment_username: weblabdeusto
              http_experiment_password: {password}
"""

WEBLAB_CONFIG_LAB_APPEND = """
        'exp1:rlc@elives' : {
                'coord_address' : 'rlc:laboratory1@core_host',
                'manages_polling': True,
                'checkers' : ()
            },
        'exp1:opamp@elives' : {
                'coord_address' : 'opamp:laboratory1@core_host',
                'manages_polling': True,
                'checkers' : ()
            }
"""

WEBLAB_CONFIG_CORE_APPEND_1 = """
            'exp1|rlc|elives':                      'rlc1@rlc_queue',
            'exp1|opamp|elives':                    'opamp1@opamp_queue'
"""

WEBLAB_CONFIG_CORE_APPEND_2 = """
        'rlc_queue'              : ('PRIORITY_QUEUE', {}),
        'opamp_queue'            : ('PRIORITY_QUEUE', {}),
"""

def do_deploy_weblab_config(config, args):
    """
    Deploys the RLC (and eventually OpAmp) in the WebLab instance.
    Assumes that weblab is deployed in the weblab user, and that we also have access to the root user.
    :param config:
    :param args:
    :return:
    """

    con_weblab = Connection(host=args.host, user="weblab", config=config)
    con_root = Connection(host=args.host, user="root", config=config)

    if not files.contains(con_weblab, "~/deployments/elives/configuration.yml", "rlc", exact=False, escape=True):
        pass



def do_deploy_server(config, args):
    """
    Deploys the boards in the server.

    Example call:
    python3 run.py --ssh-config "/Users/lrg/Dropbox/LABSLAND/SSH_CONFIG/config.d/uvt" --host uvt-raspberry-1 --deploy --ssh-keys "/Users/lrg/Dropbox/LABSLAND/PROYECTOS/E-LIVES/TECHNICAL/KEYS/gen-deployment-key" --deployment uvt

    Note that after running this command you will still have to edit the PRODRC files and specifically to add:
      - Proper API URLs
      - Correct WebLab password.
      - Correct camera URL.

    :param config:
    :param args:
    :return:
    """

    root_con = Connection(host=args.host, user="root", config=config)

    from IPython import embed
    #embed()

    # Create an elives user if we don't have one.
    if not files.exists(root_con, "/home/elives"):
        root_con.sudo("useradd -m elives -s /bin/bash")

    # If the elives user has no authorized_keys file, we copy it from root.
    if not files.exists(root_con, "/home/elives/.ssh/authorized_keys"):
        root_con.run("mkdir /home/elives/.ssh", warn=True)
        root_con.run("chown elives /home/elives/.ssh", warn=True)
        root_con.run("cp /root/.ssh/authorized_keys /home/elives/.ssh/authorized_keys")
        root_con.run("chown elives /home/elives/.ssh/authorized_keys", warn=True)

    elives_con = Connection(host=args.host, user="elives", config=config)

    # Ensure that we have the proper apt packages.
    root_con.run("apt install redis-server python3 python3-virtualenv virtualenvwrapper supervisor git vim python3-dev apache2 -y")

    result = elives_con.run("""bash -c "source ~/.nvm/nvm.sh && nvm --version" """, warn=True)
    if 'nvm' in result.stderr:
        print("Trying to download NVM...")
        elives_con.run("wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash")

    # See if npm is installed.
    result = elives_con.run("npm --version", warn=True)
    if 'npm' in result.stderr:
        result = elives_con.run("""bash -c "source ~/.nvm/nvm.sh && nvm install stable" """)
        result = elives_con.run("""bash -c "source ~/.nvm/nvm.sh && nvm use stable" """)

    # Check that we have the deployment keys into the server already and deploy them otherwise.
    #
    if args.ssh_keys is not None:
        priv_key = os.path.join(args.ssh_keys, "id_rsa")
        pub_key = os.path.join(args.ssh_keys, "id_rsa.pub")
        if os.path.isfile(priv_key):
            root_con.sudo("mkdir /home/elives/.ssh", warn=True, user="elives")
            if not files.exists(root_con, "/home/elives/.ssh/id_rsa"):
                root_con.put(priv_key, "id_rsa", preserve_mode=True)
                # Putting it directly in ~/.ssh/id_rsa does not work.
                root_con.run("mv id_rsa /home/elives/.ssh/id_rsa")
                root_con.run("chmod 600 /home/elives/.ssh/id_rsa")
                root_con.run("chown elives:elives /home/elives/.ssh/id_rsa")
        if os.path.isfile(pub_key):
            if not files.exists(root_con, "/home/elives/.ssh/id_rsa.pub"):
                root_con.put(pub_key, "id_rsa.pub", preserve_mode=True)
                root_con.run("mv id_rsa.pub /home/elives/.ssh/id_rsa.pub")
                chmod = root_con.run("chmod 644 /home/elives/.ssh/id_rsa.pub", warn=True)
                chown = root_con.run("chown elives:elives /home/elives/.ssh/id_rsa.pub", warn=True)

    # Ensure the keys which should have first been added are registered.
    root_con.sudo("""/bin/bash -c 'eval `ssh-agent` && ssh-add' """, warn=True, user="elives")

    identities = root_con.sudo("""/bin/bash -c 'eval `ssh-agent` && ssh-add -l' """, warn=True, user="elives")
    if "no identities" in identities.stdout:
        print("WARNING: The ssh-agent has no registered identities.")

    # Clone the repository if it does not exist, update it if it does.
    if files.exists(root_con, "/home/elives/board-ls"):
        # Yet another workaround for a fabric bug.
        elives_con.run("""bash -c "cd ~/board-ls && git pull" """)
    else:
        clone = elives_con.run('git clone https://elives:tr2yDHHgFcPX_kAjp9Nt@gitlab.com/elives/board-ls.git')
        # clone = root_con.sudo('git clone git@gitlab.com:elives/board-ls.git /home/elives/board-ls', user="elives")

    # Create the virtualenv for the board-ls. We will not use mkvirtualenv because it's not available easily
    # from fabric.
    mk = root_con.sudo("mkdir /home/elives/.virtualenvs", warn=True, user="elives")
    venv = root_con.sudo('virtualenv --python /usr/bin/python3 /home/elives/.virtualenvs/board-ls', warn=True, user="elives")

    # Install requirements. All in a single ugly command as a fabric bug workaround.
    root_con.sudo("""bash -c "source /home/elives/.virtualenvs/board-ls/bin/activate && cd /home/elives/board-ls && pip install -r requirements.txt" """, user="elives")


    # Set the prodrc file depending on whether it is for the opamp or rlc.
    if not files.exists(root_con, "/home/elives/board-ls/prodrc_rlc"):
        # Ensure we have deployment and type.
        if args.deployment is None:
            raise Exception("Need to specify deployment to create the PRODRC file")
        files.append(root_con, "/home/elives/board-ls/prodrc_rlc", BOARDLS_PRODRC.format(**{"institution": args.deployment, "type": "rlc"}))
        root_con.run("chown elives /home/elives/board-ls/prodrc_rlc")
    if not files.exists(root_con, "/home/elives/board-ls/prodrc_opamp"):
        # Ensure we have deployment and type.
        if args.deployment is None:
            raise Exception("Need to specify deployment to create the PRODRC file")
        files.append(root_con, "/home/elives/board-ls/prodrc_opamp", BOARDLS_PRODRC.format(**{"institution": args.deployment, "type": "opamp"}))
        root_con.run("chown elives /home/elives/board-ls/prodrc_opamp")

    # Copy the supervisor files.
    root_con.run("cp -r /home/elives/board-ls/supervisor/. /etc/supervisor/conf.d/")

    # Update supervisor scripts.
    root_con.run("supervisorctl update")

    # Restart supervisor.
    root_con.run("supervisorctl restart all", warn=True)

    # Deploy the apache config files, if they don't exist already.
    try:
        root_con.run("cp -n /home/elives/board-ls/deployments/example/apache/labs.conf /etc/apache2/conf-available/labs.conf")
    except:
        print("Warning: labs.conf was not copied, probably because it exists already.")

    # Enable proxy and rewrite.
    root_con.run("a2enmod proxy rewrite headers")

    # Enable it.
    root_con.run("a2enconf labs.conf")

    # Restart apache.
    root_con.run("/bin/bash -c 'service apache2 restart'")

    print("DONE.")
    print("Remember to: ")
    print("- Adapt labs.conf.")



parser = argparse.ArgumentParser(description="Run scripts")
parser.add_argument('--ssh-config', help="Path to the SSH config file to use.", default=None)
parser.add_argument('--deploy', help="Do deploy. Can be either board-ls or weblab-config")
parser.add_argument('--ssh-keys', help="Path to the folder that contains the SSH keys to deploy in the remote server.")
parser.add_argument('--host', type=str, help="Host to which to apply the script.")
parser.add_argument('--user', type=str, help="Username")
parser.add_argument('--deployment', type=str, help="Deployment name. E.g., uvt, psut, ud")

args = parser.parse_args()

if args.ssh_config is not None:
    config = Config(user_ssh_path=args.ssh_config)
else:
    config = Config()

if args.deploy == "board-ls":
    do_deploy_server(config, args)
elif args.deploy == "weblab-config":
    do_deploy_weblab_config(config, args)
else:
    print("Unrecognized option for deploy.", file=sys.stderr)
    parser.print_usage(file=sys.stderr)



