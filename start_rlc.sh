#!/bin/bash

PORT=8550
WORKERS=1

_term() {
   kill -TERM "$child" 2>/dev/null
}

# When SIGTERM is sent, send it
trap _term SIGTERM


cd /home/elives/board-ls
. /home/elives/.virtualenvs/board-ls/bin/activate
. prodrc_rlc

date
export SCRIPT_NAME=/labs/rlc

#gunicorn --pid rlc.gunicorn.pid --bind 127.0.0.1:$PORT -w $WORKERS wsgi_app:application &
# gunicorn --pid rlc.gunicorn.pid --bind 127.0.0.1:$PORT -w $WORKERS -k geventwebsocket.gunicorn.workers.GeventWebSocketWorker wsgi_app:application &
gunicorn --pid rlc.gunicorn.pid --bind 127.0.0.1:$PORT -w $WORKERS --worker-class eventlet wsgi_app:application &

child=$!
wait "$child"
