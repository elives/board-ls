# It must be here to retrieve this information from the dummy
core_universal_identifier       = '6585db17-e890-40c1-84a4-2f932da05987'
core_universal_identifier_human = 'elives-mosta'

db_engine          = 'mysql'
db_host            = u'localhost'
db_port            = None # None for default
db_database        = 'ElivesWebLab'
weblab_db_username = u'weblab'
weblab_db_password = u'weblab'

debug_mode   = True

quickadmin_token = 'defaulttoken'

#########################
# General configuration #
#########################

server_hostaddress = 'weblabdeusto.leog.univ-mosta.dz'
server_admin       = u'support@labsland.com'

################################
# Admin Notifier configuration #
################################

mail_notification_enabled = False

##########################
# Sessions configuration #
##########################

core_session_type = u'Memory'

# session_sqlalchemy_engine   = u'sqlite'
# session_sqlalchemy_host     = u'localhost'
# session_sqlalchemy_username = u''
# session_sqlalchemy_password = u''

# session_lock_sqlalchemy_engine   = u'sqlite'
# session_lock_sqlalchemy_host     = u'localhost'
# session_lock_sqlalchemy_username = u''
# session_lock_sqlalchemy_password = u''

# session_redis_host = u'localhost'
# session_redis_port = 6379
# core_session_pool_id = 1
# core_alive_users_session_pool_id = 1

##############################
# Core generic configuration #
##############################
core_store_students_programs      = False
core_store_students_programs_path = 'files_stored'
core_experiment_poll_time         = 350 # seconds

force_host_name = True
core_server_url = u'https://weblabdeusto.leog.univ-mosta.dz/weblab/'

############################
# Scheduling configuration #
############################

core_coordination_impl = 'redis'

coordinator_redis_db       = None
coordinator_redis_password = None
coordinator_redis_port     = None
coordinator_redis_host     = None

# core_coordinator_db_name      = u'WebLabCoordination'
# core_coordinator_db_engine    = u'sqlite'
# core_coordinator_db_host      = u'localhost'
# core_coordinator_db_username  = u'weblab'
# core_coordinator_db_password  = u'weblab'

core_coordinator_laboratory_servers = {
    'laboratory1:laboratory1@core_host' : {
            'exp1|rlc|elives'                     : 'rlc1@rlc_queue',
            'exp1|opamp|elives'                   : 'opamp1@opamp_queue'
        },
}

core_scheduling_systems = {
        'rlc_queue'              : ('PRIORITY_QUEUE', {}),
        'opamp_queue'            : ('PRIORITY_QUEUE', {})
    }

