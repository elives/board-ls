##################################
# Laboratory Server configuration #
##################################

laboratory_assigned_experiments = {
        'exp1:dummy@Dummy experiments' : {
                'coord_address' : 'experiment1:laboratory1@core_host',
                'checkers' : ()
            },
        'exp1:rlc@elives' : {
                'coord_address' : 'rlc:laboratory1@core_host',
                'checkers' : ()
            },
        'exp1:opamp@elives' : {
                'coord_address' : 'opamp:laboratory1@core_host',
                'checkers' : ()
            }
    }
