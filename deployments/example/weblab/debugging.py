# SERVERS is used by the WebLab Monitor to gather information from these ports.
# If you open them, you'll see a Python shell.
SERVERS = [
    ('127.0.0.1','10010'),
    ('127.0.0.1','10011'),
    ('127.0.0.1','10012'),
    ('127.0.0.1','10013'),
]

BASE_URL = u''

# PORTS is used by the WebLab Bot to know what
# ports it should wait prior to start using
# the simulated clients.
PORTS = {
    'json' : [10005, 10006, 10007, 10008], 
}
