#!/usr/bin/env python
#-*-*- encoding: utf-8 -*-*-
try:
    import signal
    
    import voodoo.gen.launcher as Launcher
    
    def before_shutdown():
        print("Stopping servers...")
    
    launcher = Launcher.HostLauncher(
                '.',
                'core_host',
                (
                    Launcher.SignalWait(signal.SIGTERM),
                    Launcher.SignalWait(signal.SIGINT),
                    Launcher.RawInputWait("Press <enter> or send a sigterm or a sigint to finish\n")
                ),
                {
                    "core_process1"     : "logs/config/logging.configuration.server1.txt",
                    "core_process2"     : "logs/config/logging.configuration.server2.txt",
                    "core_process3"     : "logs/config/logging.configuration.server3.txt",
                    "core_process4"     : "logs/config/logging.configuration.server4.txt",
                    "laboratory1" : "logs/config/logging.configuration.laboratory1.txt",
                },
                before_shutdown,
                (
                     Launcher.FileNotifier("_file_notifier", "server started"),
                ),
                pid_file = 'weblab.pid',
                debugger_ports = { 
                     'core_process1' : 10010, 
                     'core_process2' : 10011, 
                     'core_process3' : 10012, 
                     'core_process4' : 10013, 
                }
            )

    launcher.launch()
except:
    import traceback
    traceback.print_exc()
    raise
