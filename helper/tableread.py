import re

import clipboard
import io

RELAY_NUMBER = 15


def extract_table():
    contents = clipboard.paste()
    if contents is None:
        raise Exception("No contents in the clipboard")

    if len(contents) == 0:
        raise Exception("Contents in the clipboard seem to be empty.")

    return process_table(contents)


def standardize_var(var: str):
    """
    Standardizes a variable name.
    :param var:
    :return:
    """

    var = var.strip()

    # Convert to lowercase.
    var = var.lower()

    # Replace | and / and \ with p
    var = var.replace("|", "p")
    var = var.replace("/", "p")
    var = var.replace("\\", "p")

    # Remove values within parenthesis.
    # (Careful, might not be compatible with older tables).
    var = re.sub(r"\([0-9]*\)", "", var)
    var = var.strip()

    # Remove parentheses.
    var = var.replace("(", "")
    var = var.replace(")", "")

    # Remove spaces.
    var = var.replace(" ", "")

    return var


def standardize_relay(relay: str):
    """
    Standardizes a relay value.
    :param relay:
    :return:
    """
    if relay == '0' or relay == 0:
        return 0
    elif relay == '1' or relay == 1:
        return 1
    else:
        raise Exception("Unrecognized relay value: {}".format(relay))


def process_table_old(contents):
    """
    We use \r as a separator, and simply split rows when a value stops being a number.
    :param contents:
    :return:
    """
    cells = contents.split('\r')
    rows = []
    last_was_number = False
    current_row = []
    num_cols = 0
    for cell in cells:
        try:
            relay = standardize_relay(cell)
        except:
            relay = None

        if relay is not None:
            last_was_number = True

        else:
            if last_was_number:
                # New row.
                rows.append(current_row)
                num_cols = len(current_row)
                current_row = []
            last_was_number = False

        current_row.append(cell)

    print("We have extracted {} rows with {} columns.".format(len(rows), num_cols))

    if num_cols < 17:

        print("Contents are: {}".format(contents))

        raise Exception("Error. There are less than 17 rows, which seems too little.")

    board = input("Please type the board name: ")
    subcircuit = input("Please type the subcircuit name: ")

    gen = io.StringIO()

    for row in rows:
        relays = row[-16:]
        vars = row[0:-16]

        relays = [standardize_relay(relay) for relay in relays]
        vars = [standardize_var(var) for var in vars]

        var_str = "_".join(vars)
        key = f"{board}:{subcircuit}:{var_str}"

        gen.write(f"\"{key}\": {relays},\n")

    genstr = gen.getvalue()

    print("GENERATED: \n{}".format(genstr))


def process_table(contents):
    """
    We use \r as a row separator and \t as a cell separator. In the previous version it was different.
    :param contents:
    :return:
    """
    rows = contents.split('\r')

    saved_rows = []

    for row in rows:

        cells = row.split('\t')
        saved_cells = []

        for cell in cells:

            cell = cell.strip()
            if len(cell) == 0:
                continue

            try:
                relay = standardize_relay(cell)
            except:
                relay = None

            saved_cells.append(cell)

        saved_rows.append(saved_cells)

    num_cols = len(saved_rows[0])
    print("We have extracted {} rows with {} columns.".format(len(saved_rows), num_cols))

    if num_cols < RELAY_NUMBER:

        print("Contents are: {}".format(contents))

        raise Exception("Error. There are less than {} cols, which seems too little.".format(RELAY_NUMBER))

    board = input("Please type the board name: ")
    subcircuit = input("Please type the subcircuit name: ")

    gen = io.StringIO()

    for row in saved_rows:
        relays = row[-RELAY_NUMBER:]
        vars = row[0:-RELAY_NUMBER]

        relays = [standardize_relay(relay) for relay in relays]
        vars = [standardize_var(var) for var in vars]

        var_str = "_".join(vars)
        key = f"{board}:{subcircuit}:{var_str}"

        gen.write(f"\"{key}\": {relays},\n")

    genstr = gen.getvalue()

    print("GENERATED: \n{}".format(genstr))






if __name__ == "__main__":
    extract_table()

