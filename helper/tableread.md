
# tableread.py

This module is meant as a helper to convert from tables such as the word-based ones
that define the relay states to the Python tables in views.py.

## Intended usage

1. Copy the tables into the clipboard.
2. Run helper.tableread.extract_table()