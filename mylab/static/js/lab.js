window.namespace = '/mylab';

window.socket = undefined;
/*window.socket = io.connect(location.protocol + '//' + document.domain + ':' + location.port + namespace, {
    path: getUrl("socket.io")
});*/

window.webcam = new RefreshingCameraWidget('webcam', window.cameraUrl);
webcam.startRefreshing();
console.log("Starting refreshing.");

// Continuously ask for oscilloscope measurements.
window.oscilloscopeUpdater = new Weblab.OscilloscopeUpdater();
window.oscilloscopeUpdater.start();
window.oscilloscopeUpdater.on('measurement', onOscilloscopeMeasurement);
window.oscilloscopeUpdater.on('error', onOscilloscopeError);

window._countdownTimer = undefined;

// Continously update the components status.
window.componentsStatusInterval = setInterval(updateComponentsStatus, 1000);

/**
 * Callback supposedly invoked when the experiment ends (the name of the callback is defined in views).
 */
function onReservationFinished() {
    console.debug("Reservation finished. Redirecting to BACK URL (" + window.BACK_URL + ")");
    redirectBack();
}

function initializeCircuitChoosingDropdown() {
    var $dropdown = $(".circuit-choosing-dropdown");
    var $as = $dropdown.find("ul a");

    // Add the initial text.
    var firstElementText = $as[0].text;
    $("#choose-circuit-text").text(firstElementText);

    // Add the click listeners.
    $as.on('click', chooseCircuit);

    // Try to select first the circuit that matches the current circuit.
    $as = $dropdown.find("ul a[data-subcircuit='" + window.subcircuit + "']");
    $("#choose-circuit-text").text($as.text());
}

/**
 * Invoked when users click on the subcircuit dropdown to choose a new subcircuit.
 * @param event
 * @returns {boolean}
 */
function chooseCircuit(event) {
    $a = $(event.target);

    var board = $a.data('board');
    var subcircuit = $a.data('subcircuit');
    var text = $a.text();

    // Change the text.
    $("#choose-circuit-text").text(text);

    // Load the new circuit.
    initializeCircuit(board, subcircuit);

    // Send the config to the server to ensure it is the expected one.
    sendCurrentConfig();

    // Close the dropdown.
    $(".circuit-choosing-dropdown .dropdown-toggle").dropdown("toggle");

    // To prevent following the href, which scrolls up the page.
    return false;
}

/**
 * Utility function to scroll to a target.
 */
function scrollToTargetAdjusted(elem, offset) {
    var rawelem = undefined;
    if (elem instanceof jQuery) {
        if (elem.length > 0) {
            rawelem = elem[0];
        }
    } else {
        rawelem = elem;
    }

    if (!rawelem) {
        return;
    }

    var element = rawelem;
    var elementPosition = element.getBoundingClientRect().top;
    var offsetPosition = elementPosition + offset;

    window.scrollTo({
        top: offsetPosition,
        behavior: "smooth"
    });
}


/**
 * Invoked when the oscilloscope reports en error.
 * @param measurement
 */
function onOscilloscopeError(measurement) {
    window.oscilloscopeOnline = false;
}

/**
 * Invoked when a new oscilloscope measurement is available.
 * @param measurement
 */
function onOscilloscopeMeasurement(measurement) {
    console.debug('[onOscilloscopeMeasurement]');
    var oscilloscopeMeasurement = measurement.measurement;
    console.debug(measurement);
    if (oscilloscopeMeasurement) {
        var visirResponse = "<protocol version=\"1.3\">\n<response>\n" + oscilloscopeMeasurement + "</response></protocol>";

        // For debugging purposes, check if it's the same as the previous one.
        if (visirResponse === visir.lastMeasurement) {
            console.debug("Updating measurement with the same one.");
        } else {
            console.debug("Updating measurement with a different one.");
        }

        visir.lastMeasurement = visirResponse;

        // TO-DO: Keep better track of when it is initialized.
        try {
            window.visirOscilloscope.ReadResponse(visirResponse);
        } catch (ex) {
            console.debug("Could not set VISIR UI to provided response. Maybe VISIR has not been initialized yet.");
        }

        try {

            // Extract the particular wave so that we can check whether we are obtaining different ones.
            var parser = new DOMParser();
            var doc = parser.parseFromString(visirResponse, "text/xml");
            var channels = doc.getElementsByTagName("oscilloscope")[0].getElementsByTagName("channels")[0].getElementsByTagName("channel");

            var thereWasChange = false;

            $.each(channels, function (index, value) {
                var sample = value.getElementsByTagName("chan_samples")[0];
                var sampleText = sample.textContent;
                var num = value.getAttribute("number");

                if (num === "1") {
                    if(sampleText !== window.oscilloscopeFirstChannelMeasurement) {
                        thereWasChange = true;
                        window.oscilloscopeFirstChannelMeasurement = sampleText;
                    }
                } else if (num === "2") {
                    if(sampleText !== window.oscilloscopeSecondChannelMeasurement) {
                        thereWasChange = true;
                        window.oscilloscopeSecondChannelMeasurement = sampleText;
                    }
                } else {
                    console.debug("Unrecognized channel");
                }
            });

            // The measurement is different from last time.
            if(thereWasChange) {
                window.oscilloscopeLastMeasurementChange = new Date().getTime();
            }

            // If we got a new measurement in the last 10 seconds the oscilloscope is online, otherwise it is
            // failing.
            if(new Date().getTime() < window.oscilloscopeLastMeasurementChange + 10*1000) {
                window.oscilloscopeOnline = true;
            } else {
                window.oscilloscopeOnline = false;
                window.oscilloscopeErrorCode = "measurement-not-changing";
                window.oscilloscopeErrorMessage = "Failing. Measurement is not changing.";
            }
        } catch(e) {
            console.error("Exception caught trying to parse oscilloscope response: ");
            console.error(e);

            window.oscilloscopeOnline = false;

            window.oscilloscopeErrorCode = "unexpected-response";
            window.oscilloscopeErrorMessage = "Failing. Unexpected response from oscilloscope.";
        }

    }
}

/**
 * Redirects back to the BACK_URL.
 */
function redirectBack() {
    console.debug("Redirecting to back url");
    window.location.replace(window.BACK_URL);
}

function clean() {
    $("#panel").hide();
    // No more time
    $("#timer").text(TIME_IS_OVER);
    running = false;
    currentTime = 0;

    if (window.TIMER_INTERVAL) {
        clearInterval(window.TIMER_INTERVAL);
    }

    redirectBack();
}


function initializeCountdownTimer() {
    window._countdownTimer = $("#timer").countdown360({
        radius: 55.5,
        seconds: currentTime,
        strokeWidth: 15,
        fillStyle: '#0276FD',
        strokeStyle: '#003F87',
        fontSize: 40,
        fontColor: '#FFFFFF',
        autostart: false,
        onComplete: function () {
            console.log('Experiment finished.');
            clean();
        }
    });

    window._countdownTimer.start();
}


function updateTime() {

    if(window._countdownTimer === undefined) {
        initializeCountdownTimer();
    }

    currentTime = currentTime - 1;

    // currentTime = currentTime - 1;
    // if (currentTime > 0) {
    //     // Still time
    //     if (currentTime > 1)
    //         $("#timer").text(SECONDS_PLURAL.replace("NUM", currentTime));
    //     else
    //         $("#timer").text(SECONDS_SING);
    // } else {
    //     clean();
    // }
}

initializeCountdownTimer();
updateTime();


// Unused. I am just keeping it here as an example.
if (socket) {
    socket.on('board-status', function (data) {
    });

    socket.on('on-task', function (data) {
    });
}

function logout() {
    $.post(LOGOUT_URL, {
        csrf: CSRF
    }).done(function () {
        clean();
    });
}

/**
 * Displays an error message in the alert box for that purpose.
 * @param msg
 */
function displayError(msg) {
    if (!msg || msg === "") {
        hideErrors();
        return;
    }

    $(".row.errors-row").show();
    var $msg = $(".row.errors-row span.msg");
    $msg.text(msg);
}

/**
 * Hides back the error alert box.
 */
function hideErrors() {
    $(".row.errors-row").hide();
}

/**
 * Updates the components status table.
 */
function updateComponentsStatus() {
    if(window.interfaceServerOnline) {
        $("#status-interface-server .icon i").attr("class", "glyphicon glyphicon-ok");
        $("#status-interface-server .description").text("Online");
        $("#status-interface-server").attr("class", "success");
    } else {
        $("#status-interface-server .icon i").attr("class", "glyphicon glyphicon-remove");
        $("#status-interface-server .description").text("Offline");
        $("#status-interface-server").attr("class", "error");
    }

    if(window.instrumentsServerOnline) {
        $("#status-instruments-server .icon i").attr("class", "glyphicon glyphicon-ok");
        $("#status-instruments-server .description").text("Online");
        $("#status-instruments-server").attr("class", "success");
    } else {
        $("#status-instruments-server .icon i").attr("class", "glyphicon glyphicon-remove");
        $("#status-instruments-server .description").text("Offline");
        $("#status-instruments-server").attr("class", "error");
    }

    if(window.oscilloscopeOnline) {
        $("#status-oscilloscope .icon i").attr("class", "glyphicon glyphicon-ok");
        $("#status-oscilloscope .description").text("Online");
        $("#status-oscilloscope").attr("class", "success");
    } else {
        $("#status-oscilloscope .icon i").attr("class", "glyphicon glyphicon-remove");

        if(window.oscilloscopeErrorMessage !== undefined) {
            $("#status-oscilloscope .description").text(window.oscilloscopeErrorMessage);
        } else {
            $("#status-oscilloscope .description").text("Offline");
        }

        $("#status-oscilloscope").attr("class", "error");
    }

    if(window.funcgenOnline) {
        $("#status-funcgen .icon i").attr("class", "glyphicon glyphicon-ok");
        $("#status-funcgen .description").text("Online");
        $("#status-funcgen").attr("class", "success");
    } else {
        $("#status-funcgen .icon i").attr("class", "glyphicon glyphicon-remove");
        $("#status-funcgen .description").text("Offline");
        $("#status-funcgen").attr("class", "error");
    }

    if(window.powersupplyOnline) {
        $("#status-powersupply .icon i").attr("class", "glyphicon glyphicon-ok");
        $("#status-powersupply .description").text("Online");
        $("#status-powersupply").attr("class", "success");
    } else {
        $("#status-powersupply .icon i").attr("class", "glyphicon glyphicon-remove");
        $("#status-powersupply .description").text("Offline");
        $("#status-powersupply").attr("class", "error");
    }
}


window.HIDE_MESSAGES_BOX = null;

window.TIMER_INTERVAL = setInterval(updateTime, 1000);

window.visirInstrumentRegistry = null;

function initVisir() {
    function MakeMeasurement() {
//        window.visirInstrumentRegistry.MakeRequest(transport);
    }

    trace("starting up..");

    window.visirOscilloscope = new visir.AgilentOscilloscope(1, $("#oscilloscope"), {
        MeasureCalling: MakeMeasurement,
        CheckToContinueCalling: function () {
            return true;
        }
    });
    window.visirPowerSupply = new visir.TripleDC(1, $("#powersupply"));
    window.visirFunctionGenerator = new visir.HPFunctionGenerator(1, $("#functiongenerator"));

    window.powerSupplyUpdater = new Weblab.InstrumentUpdater({
        instrument: window.visirPowerSupply,
        name: "power supply",
        url: window.API_ROOT + '/instance/powersupply'
    });
    window.powerSupplyUpdater.start();
    window.functionGeneratorUpdater = new Weblab.InstrumentUpdater({
        instrument: window.visirFunctionGenerator,
        name: "function generator",
        url: window.API_ROOT + '/instance/functiongenerator'
    });
    window.functionGeneratorUpdater.start();


    $(".measure").click(function () {
//        MakeMeasurement();
    });

    $("#showlog").click(function () {
        $("#logwindow").css("display", "block");
    });
    $("#hidelog").click(function () {
        $("#logwindow").css("display", "none");
    });

    visir.lastMeasurement = "<protocol></protocol>";
}

$(function () {
    visir.Load(initVisir, function () {
        console.log("Error loading VISIR")
    }, VISIR_PATH);
});

$(function () {
    // Initialize the circuit controls.
    initializeCircuit(window.board, window.subcircuit);

    // Initialize the dropdown that allows circuit switching.
    initializeCircuitChoosingDropdown();

    // Send the initial circuit config.
    sendCurrentConfig();

    // Update component status.
    updateComponentsStatus();
});

