

/**
 * Initializes the value for a component with the text that correspond's to the value in the dropbox.
 * @param valueInputSelector: Selector to find the value input element. Or the element directly.
 * @param dropdownSelector: Selector to find the dropdown element. Or the element directly.
 * @param value: Optional. The value to select. If undefined, then the first available value will be chosen.
 */
function initComponent(valueInputSelector, dropdownSelector, value) {

    var $valueInput;
    var $dropdown;

    // Get the valueInput. Use the selector if given one, or use the jQuery array directly if we were given one.
    if(valueInputSelector instanceof jQuery) {
        $valueInput = valueInputSelector;
    } else {
        $valueInput = $(valueInputSelector);
    }

    // Get the dropdown element. Use the selector if given one, or the jQuery array directly.
    if(dropdownSelector instanceof jQuery) {
        $dropdown = dropdownSelector;
    } else {
        $dropdown = $(dropdownSelector);
    }

    if(value) {
        // Find the text that corresponds to the value.
        var $a = $dropdown.find("a[data-value='" + value + "']");
        if ($a.length !== 1) {
            throw new Error("Dropdown selector with value " + value + " not found.");
        }
    } else {
        // We need to choose the first available one.
        var $a = $dropdown.find("a[data-value]");
        if ($a.length < 1) {
            throw new Error("The Dropdown selector does not seem to contain any field with a value attribute.");
        }
        $a = $($a[0]);
    }

    var text = $a.html();
    $valueInput.html(text);
    $valueInput.data("value", value);
}

/**
 * This callback is invoked when the configuration of the circuit components is changed.
 * That is, when the circuit relays should change.
 */
function onConfigChange() {
    sendCurrentConfig();
}

/**
 * Sends the current circuit configuration to the server, so that the relays can be set appropriately.
 */
function sendCurrentConfig() {
    // We retrieve the current config for the circuit.
    // We also send the current board and subcircuit.
    var currentCircuitConfig = getCurrentConfiguration();

    console.debug("Configuring circuit with config: " + currentCircuitConfig);

    // We send it to the server so that the relays can be updated as needed.
    $.ajax({
        method: 'post',
        url: window.API_ROOT + '/configure',
        data: {
            circuitConfig: currentCircuitConfig
        },
        error: onConfigureError,
        success: onConfigureSuccess
    }).always(function () {
    });
}

/**
 * Callback invoked when the circuit configure request fails.
 */
function onConfigureError(error) {
    console.error("Error while trying to configure the circuit.");
    console.error(error);
    displayError("The last circuit configuration attempt failed. The lab may not work as expected.");
}

/**
 * Callback invoked when the circuit configure request is successful.
 */
function onConfigureSuccess(response) {
    console.debug("Circuit configure. Response: ");
    console.debug(response);

    var success = response.success;

    if(success) {
        console.debug("Server reported successful configuration.");
        hideErrors();
    } else {
        console.error("Server reported that the configuration attempt has failed.");
        displayError("The last circuit configuration attempt failed. The lab may not work as expected.");
    }
}


/**
 * Initializes a 'value selector' element. It assumes that it has a specific
 * HTML structure.
 *
 * @param elem: jQuery object containing the div that is the value selector element.
 * @param initial: Optional. The value that will be initially selected. If not present the first value
 * will be selected by default.
 */
function initializeValueSelector(elem, initial) {

    // Ensure that elem is a jQuery div.
    if(!elem || ! (elem instanceof jQuery) || !elem.find) {
        var err = "The element that was passed as an argument does not seem to be a valid jQuery element.";
        console.error(err);
        console.error("Received: ");
        console.error(elem);
        throw new Error(err);
    }

    // Get the element that displays the currently selected value.
    // Currently defined as a child p element with the .val class.
    var $selectedValueElement = elem.find("p.val");

    // Ensure that there is an element to display the selected value.
    if($selectedValueElement.length === 0) {
        throw new Error("Could not find a p.val element to display the selected value for the value selector.");
    }

    // Get the dropdown menu options that contain the potential values.
    // Currently defined as the a elements within a list.
    var $potentialValuesElements = elem.find("li a");

    // Ensure that we have several dropdown options.
    if($potentialValuesElements.length === 0) {
        throw new Error("Could not find any dropdown option with potential values for the value selector.");
    }

    // Get the dropdown menu element.
    var $dropdownElement = elem.find("li");

    // Function that will handle click events in the dropdown menu.
    var clickHandler = function(ev) {
        console.log("CLICKED");

        var selectedValue = $(ev.target).data("value");
        var selectedDescription = $(ev.target).html();

        // Update the text and value to the new value.
        $selectedValueElement.html(selectedDescription);
        $selectedValueElement.data("value", selectedValue);

        // Notify that there has been a change in the diagram's configuration.
        onConfigChange();
    };

    // Assign click handlers to every potential option in the dropdown.
    $potentialValuesElements.each(function(index, elem) {
        var $dropdownOption = $(elem);
        $dropdownOption.click(clickHandler);
    });

    // Set the initial option to display. If initial is undefined, initComponent will choose the first available one.
    initComponent($selectedValueElement, $dropdownElement, initial);
}

/**
 * Initializes the specific circuit.
 * @param board: Name of the board. As of now, we support 'rlc' and 'opamp'.
 * @param subcircuit: Name of the subcircuit.
 */
function initializeCircuit(board, subcircuit) {

    // Ensure that the specified diagram-col exists.
    var $activeDiagram = $(".diagram-col." + board + "." + subcircuit);

    // Show the specified diagram and hide the others.
    $(".diagram-col").hide();
    $activeDiagram.show();

    // Initialize the default values of the active diagram.
    //

    // Iterate through every value selector.
    var $valueSelectors = $activeDiagram.find(".value-selector");
    $valueSelectors.each(function(index, elem) {
        var $valueSelector = $(elem);

        // Try to find the default element, and initialize it to the default.
        var $defaultElem = $valueSelector.find(".dropdown-menu li a[data-default]");
        if($defaultElem.length === 1) {
            var defaultElemValue = $defaultElem.data('value');
            initializeValueSelector($valueSelector, defaultElemValue);
        }
    });

    // Initialize the "View" buttons for each type of device.
    $(".view-oscilloscope").click(onViewOscilloscope);
    $(".view-func-gen").click(onViewFuncGen);
}

/**
 * Clicked on the view oscilloscope button.
 */
function onViewOscilloscope(event) {
    scrollToTargetAdjusted($("#instrumentframe"), 100);
}

/**
 * Clicked on the view function generator button.
 */
function onViewFuncGen(event) {
    scrollToTargetAdjusted($("#instrumentframe"), 100);
}

/**
 * Retrieves a configuration string for the currently active diagram.
 * The configuration string has the following format: <board>:<subcircuit>:<values>.
 * <values> are the list of selected values in the dropdown, separated by '_', and in document order.
 */
function getCurrentConfiguration() {

    var $activeDiagram = $(".diagram-col:visible");

    var board = $activeDiagram.data("board");
    var subcircuit = $activeDiagram.data("subcircuit");

    var $vals = $activeDiagram.find(".value-selector .val");

    var values = [];

    $vals.each(function(index, elem) {
        var $elem = $(elem);
        var val = $elem.data("value");
        values.push(val);
    });

    var string = board + ":" + subcircuit + ":" + values.join("_");

    return string;
}
