if (typeof (Weblab) === "undefined") {
    Weblab = {};
}

Weblab.InstrumentUpdater = function () {

    function InstrumentUpdater(options) {
        var self = this;
        self._options = options || {};
        console.log(options);
        if (self._options.name === undefined || self._options.url === undefined || self._options.instrument === undefined) {
            throw "Missing name or url or instrument";
        }

        self._instrument = self._options.instrument;
        self._url = self._options.url;
        self._name = self._options.name;

        self._updateTimeout = undefined;

        self._lastConfiguration = "";
    }

    InstrumentUpdater.prototype.start = function () {
        var self = this;

        var updateRequest = function () {
            var currentConfiguration = self._options.instrument.WriteRequest();
            if (self._lastConfiguration !== currentConfiguration) {
                console.log("New " + self._name + " configuration; updating server");
                $.ajax({
                    url: self._url,
                    method: 'POST',
                    data: JSON.stringify({
                        configuration: currentConfiguration
                    }),
                    contentType: 'application/json',
                    dataType: 'json'
                })
                    .done(function (data) {
                        console.debug("Received data response from funcgen: ");
                        console.debug(data);

                        if(self._name === "function generator") {
                            window.funcgenOnline = true;
                        } else if(self._name === "power supply") {
                            window.powersupplyOnline = true;
                        }
                    })
                    .fail(function () {
                        if(self._name === "function generator") {
                            window.funcgenOnline = false;
                        } else if(self._name === "power supply") {
                            window.powersupplyOnline = false;
                        }
                    })
                    .always(function () {
                        setTimeout(updateRequest, 500);
                        self._lastConfiguration = currentConfiguration;
                    });
            } else {
                setTimeout(updateRequest, 500);
            }
        };

        self._updateTimeout = setTimeout(updateRequest, 300);
    };

    InstrumentUpdater.prototype.stop = function () {
        if (self._updateTimeout !== undefined) {
            clearTimeout(self._updateTimeout);
            self._updateTimeout = undefined;
        }
    };

    return InstrumentUpdater;
}();
