///////////////////////////////////////////////////
//
// Module: weblab.oscilloscope-updater.js
// Purpose: Continuously requests data for the oscilloscope.
// Dependencies: jQuery
// Other: Assumes that the window.API_ROOT variable exists and points to the /api endpoint.
//
///////////////////////////////////////////////////


if (typeof(Weblab) === "undefined") {
    Weblab = {};
}

/**
 * Manages the oscilloscope measurements retrieval. It calls /instance/oscilloscope and retrieves measurements repeteadly
 * when active.
 */
Weblab.OscilloscopeUpdater = function () {

    /**
     * Creates the OscilloscopeUpdater object.
     *
     * Events that the object triggers:
     *   - measurement (when a measurement has been retrieved).
     *   - error (when an error occurs trying to retrieve a measurement).
     *
     * @param options Optional dictionary with configuration settings. Supported: None for now.
     * @constructor
     */
    function OscilloscopeUpdater(options) {
        var self = this;

        // Save the options dictionary.
        self._options = options || {};

        // For the custom event handlers implementation.
        self._eventHandlers = {};

        // Timeout for sending requests periodically adapting to network speed.
        self._requestTimeout = undefined;

        // Timeout for sending requests periodically updating the oscilloscope configuration
        self._updateTimeout = undefined;

        // Last oscilloscope configuration. Only when it's different to the current oscilloscope 
        // we should make a call to the server
        self._lastOscilloscopeConfiguration = "";
    }

    /**
     * Registers an event handler.
     * @param event
     * @param handler
     * @param data
     */
    OscilloscopeUpdater.prototype.on = function (event, handler, data) {
        var self = this;

        if (!_.has(self._eventHandlers, event)) {
            self._eventHandlers[event] = [];
        }

        var handlersList = self._eventHandlers[event];

        var handlerEntry = {
            handler: handler,
            eventData: data
        };

        handlersList.push(handlerEntry);
    };

    /**
     * Removes event handlers.
     * @param event Optional. Event type whose handler to remove. If undefined, all event types and handlers will be removed.
     * @param handler Optional. If undefined, all handlers will be removed.
     */
    OscilloscopeUpdater.prototype.off = function (event, handler) {
        var self = this;

        // If an event type was not specified we need to remove all handlers.
        if (event === undefined) {
            self._eventHandlers = [];
        }

        if (_.has(self._eventHandlers, event)) {
            if (!handler) {
                delete self._eventHandlers[event];
            } else {
                var handlersList = self._eventHandlers[event];
                _.remove(handlersList, function (entry) {
                    return entry.handler === handler;
                });
            }
        }
    };

    /**
     * Triggers the specific event. It is used internally.
     * @param event
     * @param eventObj Event object that contains the event data. Note: The eventObj.eventData field is overwritten.
     */
    OscilloscopeUpdater.prototype.trigger = function (event, eventObj) {
        var self = this;

        // Invoke the event.
        _.each(self._eventHandlers[event], function (entry) {
            eventObj.eventData = entry.eventData;
            entry.handler(eventObj);
        });
    };


    /**
     * Starts requesting oscilloscope measurements periodically. Does nothing if it is already requesting them.
     * Note that it will send POST requests to update the configuration if the oscilloscope configuration changes.
     *
     */
    OscilloscopeUpdater.prototype.start = function () {
        var self = this;

        if(self._requestTimeout !== undefined) {
            return;
        }

        /**
         * Handles a successful request response. Note that this callback might be invoked even after
         * the updater has been stopped, because there might have been a pending request when stop() was
         * called.
         * @param data
         */
        var onSuccess = function(data) {

            // If the result does not contain result=success, then it probably wasn't actually
            // a success.
            if(data.result !== 'success') {
                console.error('Oscilloscope measurement retrieval did not report success.');
                console.error(data);

                self.trigger('error', data);

                return;
            }

            if(data.measurement !== null) {
                // Raise a 'measurement' event so that the data can be used freely.
                self.trigger('measurement', data);
            } else {
                console.debug("Received measurement is null. This might not be an error: we might receive a valid measurement soon.");
            }

            // Request succeeded, schedule the next request.
            self._requestTimeout = setTimeout(requestMeasurement, 1000);
        };

        /**
         * Handles a failed request response. Will retry automatically after X milliseconds.
         * @param error
         */
        var onFailure = function(error) {
            console.error('Oscilloscope measurement retrieval request seems to have failed.');
            console.error(error);

            // Request failed, schedule the next attempt.
            self._requestTimeout = setTimeout(requestMeasurement, 1000);

            // Trigger an error.
            self.trigger('error', error);
        };

        var requestMeasurement = function() {
            $.ajax({
                url: window.API_ROOT + '/instance/oscilloscope',
                method: 'GET'
            })
                .done(onSuccess)
                .fail(onFailure);
        };

        // Do request for the first time.
        self._requestTimeout = setTimeout(requestMeasurement, 1000);

        var updateOscilloscopeRequest = function () {

            // Bug fix: When we get here, visirOscilloscope might have not been initialized yet.
            // Support that case.

            var currentOscilloscopeConfiguration;

            if(window.visirOscilloscope) {
                currentOscilloscopeConfiguration = window.visirOscilloscope.WriteRequest();
            } else {
                currentOscilloscopeConfiguration = undefined;
                self._updateTimeout = setTimeout(updateOscilloscopeRequest, 500);
                return;
            }

            if (self._lastOscilloscopeConfiguration !== currentOscilloscopeConfiguration) {
                console.log("New oscilloscope configuration; updating server");

                // We include the board and subcircuit here, so that we can bind virtual to physical channels.

                $.ajax({
                    url: window.API_ROOT + '/instance/oscilloscope',
                    method: 'POST',
                    data: JSON.stringify({
                        configuration: currentOscilloscopeConfiguration
                    }),
                    contentType: 'application/json',
                    dataType: 'json'
                }).always(function () {
                    self._updateTimeout = setTimeout(updateOscilloscopeRequest, 500);
                    self._lastOscilloscopeConfiguration = currentOscilloscopeConfiguration;
                });
            } else {
                self._updateTimeout = setTimeout(updateOscilloscopeRequest, 500);
            }
        };

        self._updateTimeout = setTimeout(updateOscilloscopeRequest, 300);
    };

    /**
     * Stops requesting oscilloscope measurements periodically. Does nothing if it is already requesting them.
     */
    OscilloscopeUpdater.prototype.stop = function() {
        if(self._requestTimeout !== undefined) {
            clearTimeout(self._requestTimeout);
            self._requestTimeout = undefined;
        }
        if(self._updateTimeout !== undefined) {
            clearTimeout(self._updateTimeout);
            self._updateTimeout = undefined;
        }
    };


    return OscilloscopeUpdater;

}();
