import time
import sys
import requests

import logging
logger = logging.getLogger(__name__)

from flask import Blueprint, url_for, render_template, jsonify, session, current_app, request
from flask_socketio import emit

from mylab import weblab, socketio

from weblablib import requires_active, requires_login, socket_requires_active, weblab_user, logout

main_blueprint = Blueprint('main', __name__)

# Large dictionary that indicates to which physical oscilloscope channel we bind our virtual oscilloscope channels.
# E.g., for a particular subcircuit, virtual oscilloscope channel 2 might actually be connected physically to the oscilloscope channel 4.
# Note: The channel index here is 0-based.
OSCILLOSCOPE_BINDINGS_VIRTUAL_TO_PHYSICAL = {
    'rlc:resistor-resistor': [0, 1],
    'rlc:capacitor-resistor': [0, 1],
    'rlc:inductor-resistor': [0, 1]
}

# Large dictionary that links each configuration and state with the relay settings.
CONFIG_TO_RELAYS_DICTIONARY = {

    # region RLC REGION

    # The following RLC combinations are an implementation of Abdelhalim's TechSpec V4. Previous versions of the server
    # implemented V3.


    # RLC R-R Potentiometric Divider (Experiment 1.1) (v4.0)
    "rlc:r-r-potentiometric-divider:r1_r3": [1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1],
    "rlc:r-r-potentiometric-divider:r1_r4": [1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 1],
    "rlc:r-r-potentiometric-divider:r1_r3ppr4": [1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 1, 1],
    "rlc:r-r-potentiometric-divider:r2_r3": [1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1],
    "rlc:r-r-potentiometric-divider:r2_r4": [1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1],
    "rlc:r-r-potentiometric-divider:r2_r3ppr4": [1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1],
    "rlc:r-r-potentiometric-divider:r1ppr2_r3": [1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1],
    "rlc:r-r-potentiometric-divider:r1ppr2_r4": [1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 0, 1, 1],
    "rlc:r-r-potentiometric-divider:r1ppr2_r3ppr4": [1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 1, 1],

    # RLC C-C Potentiometric Divider (Experiment 1.2) (v4.0)
    "rlc:c-c-potentiometric-divider:c1_c3": [1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1],
    "rlc:c-c-potentiometric-divider:c1_c4": [1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1],
    "rlc:c-c-potentiometric-divider:c1_c3ppc4": [1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1],
    "rlc:c-c-potentiometric-divider:c2_c3": [1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1],
    "rlc:c-c-potentiometric-divider:c2_c4": [1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1],
    "rlc:c-c-potentiometric-divider:c2_c3ppc4": [1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1],
    "rlc:c-c-potentiometric-divider:c1ppc2_c3": [1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1],
    "rlc:c-c-potentiometric-divider:c1ppc2_c4": [1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1],
    "rlc:c-c-potentiometric-divider:c1ppc2_c3ppc4": [1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1],

    # RLC L-L Potentiometric Divider (Experiment 1.3) (v4.0)
    "rlc:l-l-potentiometric-divider:l1_l3": [1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1],
    "rlc:l-l-potentiometric-divider:l1_l4": [1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1],
    "rlc:l-l-potentiometric-divider:l1_l3ppl4": [1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1],
    "rlc:l-l-potentiometric-divider:l2_l3": [0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1],
    "rlc:l-l-potentiometric-divider:l2_l4": [0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1],
    "rlc:l-l-potentiometric-divider:l2_l3ppl4": [0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1],
    "rlc:l-l-potentiometric-divider:l1ppl2_l3": [0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1],
    "rlc:l-l-potentiometric-divider:l1ppl2_l4": [0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1],
    "rlc:l-l-potentiometric-divider:l1ppl2_l3ppl4": [0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1],

    # RLC Resistor Capacitor (Experiment 2) (v4.0)
    "rlc:resistor-capacitor:r1_c3": [1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1],
    "rlc:resistor-capacitor:r1_c4": [1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 1],
    "rlc:resistor-capacitor:r1_c3ppc4": [1, 1, 1, 1, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 1],
    "rlc:resistor-capacitor:r2_c3": [1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1],
    "rlc:resistor-capacitor:r2_c4": [1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1],
    "rlc:resistor-capacitor:r2_c3ppc4": [1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1],
    "rlc:resistor-capacitor:r1ppr2_c3": [1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1],
    "rlc:resistor-capacitor:r1ppr2_c4": [1, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 0, 1, 1],
    "rlc:resistor-capacitor:r1ppr2_c3ppc4": [1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 1, 1],

    # RLC Resistor Inductor (Experiment 2 by mistake) (Experiment 3) (v4.0)
    "rlc:resistor-inductor:r1_l3": [1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0, 1, 1],
    "rlc:resistor-inductor:r1_l4": [1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1],
    "rlc:resistor-inductor:r1_l3ppl4": [1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0, 1, 1],
    "rlc:resistor-inductor:r2_l3": [1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1],
    "rlc:resistor-inductor:r2_l4": [1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1],
    "rlc:resistor-inductor:r2_l3ppl4": [1, 1, 1, 1, 0, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1],
    "rlc:resistor-inductor:r1ppr2_l3": [1, 1, 1, 1, 0, 0, 1, 0, 1, 1, 1, 1, 0, 1, 1],
    "rlc:resistor-inductor:r1ppr2_l4": [1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1],
    "rlc:resistor-inductor:r1ppr2_l3ppl4": [1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 1],

    # RLC Capacitor Resistor (Experiment 1 by mistake) (Experiment 4) (v4.0)
    "rlc:capacitor-resistor:c1_r3": [1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1],
    "rlc:capacitor-resistor:c1_r4": [1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1],
    "rlc:capacitor-resistor:c1_r3ppr4": [1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1],
    "rlc:capacitor-resistor:c2_r3": [1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1],
    "rlc:capacitor-resistor:c2_r4": [1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1],
    "rlc:capacitor-resistor:c2_r3ppr4": [1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1],
    "rlc:capacitor-resistor:c1ppc2_r3": [1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1],
    "rlc:capacitor-resistor:c1ppc2_r4": [1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1],
    "rlc:capacitor-resistor:c1ppc2_r3ppr4": [1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1],

    # RLC Inductor Resistor (Experiment 6 by mistake) (Experiment 5) (v4.0)
    "rlc:inductor-resistor:l1_r3": [1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1],
    "rlc:inductor-resistor:l1_r4": [1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1],
    "rlc:inductor-resistor:l1_r3ppr4": [1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1],
    "rlc:inductor-resistor:l2_r3": [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1],
    "rlc:inductor-resistor:l2_r4": [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1],
    "rlc:inductor-resistor:l2_r3ppr4": [0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1],
    "rlc:inductor-resistor:l1ppl2_r3": [0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1],
    "rlc:inductor-resistor:l1ppl2_r4": [0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1],
    "rlc:inductor-resistor:l1ppl2_r3ppr4": [0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1],

    # RLC L-C-R (Experiment 7 by mistake) (Experiment 6) (v4.0)
    "rlc:l-c-r:l1_c3": [1, 0, 1, 1, 1, 1, 1, 1, 1, 0,  1, 1, 1, 0, 0],
    "rlc:l-c-r:l1_c4": [1, 0, 1, 1, 1, 1, 1, 1, 0, 1,  1, 1, 1, 0, 0],
    "rlc:l-c-r:l1_c3ppc4": [1, 0, 1, 1, 1, 1, 1, 1, 0, 0,  1, 1, 1, 0, 0],
    "rlc:l-c-r:l2_c3": [0, 1, 1, 1, 1, 1, 1, 1, 1, 0,  1, 1, 1, 0, 0],
    "rlc:l-c-r:l2_c4": [0, 1, 1, 1, 1, 1, 1, 1, 0, 1,  1, 1, 1, 0, 0],
    "rlc:l-c-r:l2_c3ppc4": [0, 1, 1, 1, 1, 1, 1, 1, 0, 0,  1, 1, 1, 0, 0],
    "rlc:l-c-r:l1ppl2_c3": [0, 0, 1, 1, 1, 1, 1, 1, 1, 0,  1, 1, 1, 0, 0],
    "rlc:l-c-r:l1ppl2_c4": [0, 0, 1, 1, 1, 1, 1, 1, 0, 1,  1, 1, 1, 0, 0],
    "rlc:l-c-r:l1ppl2_c3ppc4": [0, 0, 1, 1, 1, 1, 1, 0, 0,  0, 1, 1, 1, 0, 0],

    # RLC C-L-R (Experiment 8 by mistake) (Experiment 7) (v4.0)
    "rlc:c-l-r:c1_l3": [1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0],
    "rlc:c-l-r:c1_l4": [1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 0],
    "rlc:c-l-r:c1_l3ppl4": [1, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0],
    "rlc:c-l-r:c2_l3": [1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0],
    "rlc:c-l-r:c2_l4": [1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 0],
    "rlc:c-l-r:c2_l3ppl4": [1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0],
    "rlc:c-l-r:c1ppc2_l3": [1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0],
    "rlc:c-l-r:c1ppc2_l4": [1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 0],
    "rlc:c-l-r:c1ppc2_l3ppl4": [1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0],

    #endregion RLC REGION



    # NOTE:
    # OpAmp circuits with v1.5 implement Abdelhalim's OpAmp spec v1.5.

    # region OPAMP REGION

    # OpAmp Inverting Amplifier (Circuit 1, Experiment 1, v1.5)
    "opamp:inverting-amplifier:r1_r5": [1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0],
    "opamp:inverting-amplifier:r2_r5": [1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0],
    "opamp:inverting-amplifier:r1ppr2_r5": [1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0],
    "opamp:inverting-amplifier:r1+r4_r5": [1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0],
    "opamp:inverting-amplifier:r2+r4_r5": [1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0],
    "opamp:inverting-amplifier:r1ppr2+r4_r5": [1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0],
    "opamp:inverting-amplifier:r1_r6": [1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1],
    "opamp:inverting-amplifier:r2_r6": [1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1],
    "opamp:inverting-amplifier:r1ppr2_r6": [1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1],
    "opamp:inverting-amplifier:r1+r4_r6": [1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 0, 1],
    "opamp:inverting-amplifier:r2+r4_r6": [1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 0, 1],
    "opamp:inverting-amplifier:r1ppr2+r4_r6": [1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 0, 1],
    "opamp:inverting-amplifier:r1_r5ppr6": [1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 0, 0],
    "opamp:inverting-amplifier:r2_r5ppr6": [1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 0],
    "opamp:inverting-amplifier:r1ppr2_r5ppr6": [1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 1, 0, 0],
    "opamp:inverting-amplifier:r1+r4_r5ppr6": [1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 0, 0],
    "opamp:inverting-amplifier:r2+r4_r5ppr6": [1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 0, 0],
    "opamp:inverting-amplifier:r1ppr2+r4_r5ppr6": [1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 0, 0],
    "opamp:inverting-amplifier:r1_r5+r8": [1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0],
    "opamp:inverting-amplifier:r2_r5+r8": [1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0],
    "opamp:inverting-amplifier:r1ppr2_r5+r8": [1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0],
    "opamp:inverting-amplifier:r1+r4_r5+r8": [1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0],
    "opamp:inverting-amplifier:r2+r4_r5+r8": [1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0],
    "opamp:inverting-amplifier:r1ppr2+r4_r5+r8": [1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0],
    "opamp:inverting-amplifier:r1_r6+r8": [1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1],
    "opamp:inverting-amplifier:r2_r6+r8": [1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1],
    "opamp:inverting-amplifier:r1ppr2_r6+r8": [1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1],
    "opamp:inverting-amplifier:r1+r4_r6+r8": [1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1],
    "opamp:inverting-amplifier:r2+r4_r6+r8": [1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1],
    "opamp:inverting-amplifier:r1ppr2+r4_r6+r8": [1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1],
    "opamp:inverting-amplifier:r1_r5ppr6+r8": [1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 1, 0, 0],
    "opamp:inverting-amplifier:r2_r5ppr6+r8": [1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 0],
    "opamp:inverting-amplifier:r1ppr2_r5ppr6+r8": [1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 1, 0, 0],
    "opamp:inverting-amplifier:r1+r4_r5ppr6+r8": [1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0],
    "opamp:inverting-amplifier:r2+r4_r5ppr6+r8": [1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0],
    "opamp:inverting-amplifier:r1ppr2+r4_r5ppr6+r8": [1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0],


    # OpAmp Non Inverting Amplifier (Circuit 2, Experiment 2, v1.5)
    "opamp:non-inverting-amplifier:r1_r5": [0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 1, 0],
    "opamp:non-inverting-amplifier:r2_r5": [0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 1, 0, 1, 1, 1, 0],
    "opamp:non-inverting-amplifier:r1ppr2_r5": [0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 1, 0],
    "opamp:non-inverting-amplifier:r1+r4_r5": [0, 1, 1, 1, 0, 0, 0, 1, 0, 0, 1, 0, 1, 1, 1, 0],
    "opamp:non-inverting-amplifier:r2+r4_r5": [0, 0, 1, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 0],
    "opamp:non-inverting-amplifier:r1ppr2+r4_r5": [0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 1, 0, 1, 1, 1, 0],
    "opamp:non-inverting-amplifier:r1_r6": [0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 0, 1],
    "opamp:non-inverting-amplifier:r2_r6": [0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 1, 0, 1, 1, 0, 1],
    "opamp:non-inverting-amplifier:r1ppr2_r6": [0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 0, 1],
    "opamp:non-inverting-amplifier:r1+r4_r6": [0, 1, 1, 1, 0, 0, 0, 1, 0, 0, 1, 0, 1, 1, 0, 1],
    "opamp:non-inverting-amplifier:r2+r4_r6": [0, 0, 1, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1, 1, 0, 1],
    "opamp:non-inverting-amplifier:r1ppr2+r4_r6": [0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 1, 0, 1, 1, 0, 1],
    "opamp:non-inverting-amplifier:r1_r5ppr6": [0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 0, 0],
    "opamp:non-inverting-amplifier:r2_r5ppr6": [0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 1, 0, 1, 1, 0, 0],
    "opamp:non-inverting-amplifier:r1ppr2_r5ppr6": [0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 1, 1, 0, 0],
    "opamp:non-inverting-amplifier:r1+r4_r5ppr6": [0, 1, 1, 1, 0, 0, 0, 1, 0, 0, 1, 0, 1, 1, 0, 0],
    "opamp:non-inverting-amplifier:r2+r4_r5ppr6": [0, 0, 1, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1, 1, 0, 0],
    "opamp:non-inverting-amplifier:r1ppr2+r4_r5ppr6": [0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 1, 0, 1, 1, 0, 0],
    "opamp:non-inverting-amplifier:r1_r5+r8": [0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0],
    "opamp:non-inverting-amplifier:r2_r5+r8": [0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0],
    "opamp:non-inverting-amplifier:r1ppr2_r5+r8": [0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0],
    "opamp:non-inverting-amplifier:r1+r4_r5+r8": [0, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 0],
    "opamp:non-inverting-amplifier:r2+r4_r5+r8": [0, 0, 1, 1, 0, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1, 0],
    "opamp:non-inverting-amplifier:r1ppr2+r4_r5+r8": [0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 0],
    "opamp:non-inverting-amplifier:r1_r6+r8": [0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 0, 1],
    "opamp:non-inverting-amplifier:r2_r6+r8": [0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 1],
    "opamp:non-inverting-amplifier:r1ppr2_r6+r8": [0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 0, 1],
    "opamp:non-inverting-amplifier:r1+r4_r6+r8": [0, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 1],
    "opamp:non-inverting-amplifier:r2+r4_r6+r8": [0, 0, 1, 1, 0, 1, 0, 1, 0, 0, 0, 1, 1, 1, 0, 1],
    "opamp:non-inverting-amplifier:r1ppr2+r4_r6+r8": [0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 1],
    "opamp:non-inverting-amplifier:r1_r5ppr6+r8": [0, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0],
    "opamp:non-inverting-amplifier:r2_r5ppr6+r8": [0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0],
    "opamp:non-inverting-amplifier:r1ppr2_r5ppr6+r8": [0, 0, 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0],
    "opamp:non-inverting-amplifier:r1+r4_r5ppr6+r8": [0, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0],
    "opamp:non-inverting-amplifier:r2+r4_r5ppr6+r8": [0, 0, 1, 1, 0, 1, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0],
    "opamp:non-inverting-amplifier:r1ppr2+r4_r5ppr6+r8": [0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0],

    # OpAmp Integrator (Circuit 3, Experiment 3, v1.5)
    "opamp:integrator:r1": [1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1],
    "opamp:integrator:r2": [1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1],
    "opamp:integrator:r1ppr2": [1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1],
    "opamp:integrator:r1+r4": [1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 0, 1, 1],
    "opamp:integrator:r2+r4": [1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 1],
    "opamp:integrator:r1ppr2+r4": [1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 0, 1, 1],

    # OpAmp Differentiator (Circuit 4, Experiment 4, v1.5)
    "opamp:differentiator:r5": [1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0],
    "opamp:differentiator:r6": [1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1],
    "opamp:differentiator:r5ppr6": [1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 0],
    "opamp:differentiator:r5+r8": [1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0],
    "opamp:differentiator:r6+r8": [1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1],
    "opamp:differentiator:r5ppr6+r8": [1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 0],

    # Low-Pass Filter (Circuit 5, Experiment 5, v1.5)
    "opamp:low-pass-filter:r1_r5": [1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0],
    "opamp:low-pass-filter:r2_r5": [1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0],
    "opamp:low-pass-filter:r1ppr2_r5": [1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0],
    "opamp:low-pass-filter:r1+r4_r5": [1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 0, 1, 0],
    "opamp:low-pass-filter:r2+r4_r5": [1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 0],
    "opamp:low-pass-filter:r1ppr2+r4_r5": [1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 0, 1, 0],
    "opamp:low-pass-filter:r1_r6": [1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 0, 1],
    "opamp:low-pass-filter:r2_r6": [1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 0, 1],
    "opamp:low-pass-filter:r1ppr2_r6": [1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 0, 1],
    "opamp:low-pass-filter:r1+r4_r6": [1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 1],
    "opamp:low-pass-filter:r2+r4_r6": [1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 0, 1],
    "opamp:low-pass-filter:r1ppr2+r4_r6": [1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 1],
    "opamp:low-pass-filter:r1_r5ppr6": [1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 0, 0],
    "opamp:low-pass-filter:r2_r5ppr6": [1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 0, 0],
    "opamp:low-pass-filter:r1ppr2_r5ppr6": [1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 0, 0],
    "opamp:low-pass-filter:r1+r4_r5ppr6": [1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0],
    "opamp:low-pass-filter:r2+r4_r5ppr6": [1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0],
    "opamp:low-pass-filter:r1ppr2+r4_r5ppr6": [1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0],

    # High-Pass Filter (Circuit 6, Experiment 6, v1.5)
    "opamp:high-pass-filter:r5": [1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0],
    "opamp:high-pass-filter:r6": [1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1],
    "opamp:high-pass-filter:r5ppr6": [1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 0],
    "opamp:high-pass-filter:r5+r8": [1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0],
    "opamp:high-pass-filter:r6+r8": [1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1],
    "opamp:high-pass-filter:r5ppr6+r8": [1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 0],

    # Band-Pass Filter (Circuit 7, Experiment 7, v1.5)
    "opamp:band-pass-filter:r5": [1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0],
    "opamp:band-pass-filter:r6": [1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 0, 1],
    "opamp:band-pass-filter:r5ppr6": [1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 0, 0],

    # Band-Stop Filter One (Circuit 8, Experiment 8.1, v1.5)
    "opamp:band-stop-filter-one:r1_r5": [1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 0, 1, 0],
    "opamp:band-stop-filter-one:r2_r5": [1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 0],
    "opamp:band-stop-filter-one:r1ppr2_r5": [1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 0, 1, 0],
    "opamp:band-stop-filter-one:r1_r6": [1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 0, 0, 1],
    "opamp:band-stop-filter-one:r2_r6": [1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 0, 1],
    "opamp:band-stop-filter-one:r1ppr2_r6": [1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 0, 0, 1],
    "opamp:band-stop-filter-one:r1_r5ppr6": [1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 0, 0, 0],
    "opamp:band-stop-filter-one:r2_r5ppr6": [1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 0, 0],
    "opamp:band-stop-filter-one:r1ppr2_r5ppr6": [1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 0, 0, 0],

    # Band-Stop Filter Two (R4 off) (Circuit 9, Experiment 8.2, v1.5)
    "opamp:band-stop-filter-two:r1_r5": [1, 1, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 0, 1, 0],
    "opamp:band-stop-filter-two:r2_r5": [1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 0],
    "opamp:band-stop-filter-two:r1ppr2_r5": [1, 0, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 0, 1, 0],
    "opamp:band-stop-filter-two:r1_r6": [1, 1, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 0, 0, 1],
    "opamp:band-stop-filter-two:r2_r6": [1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 0, 1],
    "opamp:band-stop-filter-two:r1ppr2_r6": [1, 0, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 0, 0, 1],
    "opamp:band-stop-filter-two:r1_r5ppr6": [1, 1, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 0, 0, 0],
    "opamp:band-stop-filter-two:r2_r5ppr6": [1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 0, 0],
    "opamp:band-stop-filter-two:r1ppr2_r5ppr6": [1, 0, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 0, 0, 0],

    # Band-Stop Filter Three (R8 off) (Circuit 10, Experiment 8.3, v1.5)
    "opamp:band-stop-filter-three:r1_r5": [1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 0, 1, 0],
    "opamp:band-stop-filter-three:r2_r5": [1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 0],
    "opamp:band-stop-filter-three:r1ppr2_r5": [1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 0, 1, 0],
    "opamp:band-stop-filter-three:r1_r6": [1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 1],
    "opamp:band-stop-filter-three:r2_r6": [1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 0, 1],
    "opamp:band-stop-filter-three:r1ppr2_r6": [1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 1],
    "opamp:band-stop-filter-three:r1_r5ppr6": [1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0],
    "opamp:band-stop-filter-three:r2_r5ppr6": [1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0],
    "opamp:band-stop-filter-three:r1ppr2_r5ppr6": [1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0, 0],

    # Band-Stop Filter Four (R4+R8 off) (Circuit 11, Experiment 8.4, v1.5)
    "opamp:band-stop-filter-four:r1_r5": [1, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0],
    "opamp:band-stop-filter-four:r2_r5": [1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0],
    "opamp:band-stop-filter-four:r1ppr2_r5": [1, 0, 0, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0],
    "opamp:band-stop-filter-four:r1_r6": [1, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 0, 1],
    "opamp:band-stop-filter-four:r2_r6": [1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 0, 1],
    "opamp:band-stop-filter-four:r1ppr2_r6": [1, 0, 0, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 0, 1],
    "opamp:band-stop-filter-four:r1_r5ppr6": [1, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 0, 0],
    "opamp:band-stop-filter-four:r2_r5ppr6": [1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 0, 0],
    "opamp:band-stop-filter-four:r1ppr2_r5ppr6": [1, 0, 0, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 0, 0],

    # endregion opamp
}


@main_blueprint.route('/healthcheck')
def healthcheck():
    return "hi"


@weblab.initial_url
def initial_url():
    """
    Where do we send the user when a new user comes?
    """
    return url_for('main.index')


@main_blueprint.route('/')
@requires_login
def index():
    # This method generates a random identifier and stores it in Flask's session object
    # For any request coming from the client, we'll check it. This way, we avoid
    # CSRF attacks (check https://en.wikipedia.org/wiki/Cross-site_request_forgery )
    session['csrf'] = weblab.create_token()

    return render_template("index.html")


@weblab.on_start
def start(client_data, server_data):
    print("************************************************************************")
    print("Preparing laboratory for user {}...".format(weblab_user.username))
    print()
    print(" - Typically, here you prepare resources.")
    print(" - Since this method is run *before* the user goes to the lab, you can't")
    print("   store information on Flask's 'session'. But you can store it on:")
    print("   weblab_user.data")
    weblab_user.data['local_identifier'] = weblab.create_token()
    print("   In this case: {}".format(weblab_user.data['local_identifier']))
    print()
    print("************************************************************************")
    print("CLIENT DATA: {}".format(client_data))
    print("SERVER DATA: {}".format(server_data))

    # Initialize the board.
    board = current_app.config['BOARD_TYPE']
    if board not in ['opamp', 'rlc']:
        raise Exception('Unsupported type of board: {}'.format(board))

    # Initialize the subcircuit.
    subcircuit = weblab_user.data.get('subcircuit')
    if subcircuit is None:
        if board == 'rlc':
            subcircuit = 'r-r-potentiometric-divider'
        elif board == 'opamp':
            subcircuit = 'inverting-amplifier'
    weblab_user.data['subcircuit'] = subcircuit

    # Reset the boards.
    reset_boards()

@weblab.on_dispose
def dispose():
    # Reset the boards. This is done on start as well, but some partners have requested the relays to be
    # left in an off state after usage.
    reset_boards()




###################################################################
# 
# 
# Socket-IO management
#

@main_blueprint.route('/logout', methods=['POST'])
@requires_login
def logout_view():
    if not _check_csrf():
        return jsonify(error=True, message="Invalid JSON")

    if weblab_user.active:
        logout()

    return jsonify(error=False)


###################################
# API CALLS
###################################

@main_blueprint.route('/api', methods=['GET'])
@requires_active
def api_root():
    """
    The purpose of this endpoint is mostly to be able to retrieve the URL through Flask's url_for
    to get the API root URL.
    :return:
    """
    return jsonify(result='success', message='API')


@main_blueprint.route('/api/configure', methods=['POST'])
@requires_active
def api_configure():
    """
    Configures the circuit.
    :return:
    """

    circuit_config = request.form.get('circuitConfig')

    # Extract the board name (which we should already know) and the subcircuit name.
    splits = circuit_config.split(":")
    board = splits[0]
    subcircuit = splits[1]

    # Save internally our current subcircuit, in case we reload the page.
    weblab_user.data['subcircuit'] = subcircuit

    # Lookup in the table the matching relay configuration.
    relays = CONFIG_TO_RELAYS_DICTIONARY.get(circuit_config)

    if relays is None:
        error_msg = 'Error. Could not find matching relay configuration in the lookup table. Config: {}'.format(circuit_config)
        print(error_msg, file=sys.stderr)
        return jsonify(result='error', message=error_msg)

    # Set the relays configuration through the Interface Server.
    #
    if current_app.config['FAKE_HARDWARE']:
        from . import e2eresponses
        return jsonify(e2eresponses.RESPONSE_FOR_CONFIGURE_RELAYS)

    # TO-DO: Here we should probably update the oscilloscope virtual-real bindings.
    # Try to extract the board and subcircuit.
    virtual_to_real_oscilloscope = OSCILLOSCOPE_BINDINGS_VIRTUAL_TO_PHYSICAL.get("{}:{}".format(board, subcircuit))
    mapping_failed = False
    if virtual_to_real_oscilloscope is not None:
        print("Oscilloscope binding: {}".format(virtual_to_real_oscilloscope))

        instruments_api = current_app.config['INSTRUMENTS_SERVER_API_URL']
        target_url = instruments_api + "oscilloscope"
        request_data = {'mapped_channels': virtual_to_real_oscilloscope}

        # Request to the instruments server to map the oscilloscope channels if needed.
        r = requests.post(target_url, json=request_data)

        if r.status_code != 200:
            logger.error("Failed to configure the oscilloscope's channel mapping. Response {} with code {}.".format(r.text, r.status_code))
            mapping_failed = True


    instance_api = current_app.config['INTERFACE_SERVER_API_URL']
    target_url = instance_api + "configureRelays"
    request_data = {'config': relays}
    relays_failed = False

    # Request to the device server to map the relays if needed.
    r = requests.post(target_url, json=request_data)

    if r.status_code != 200:
        logger.error("Failed to configure the device server's relays. Response {} with code {}.".format(r.text, r.status_code))
        relays_failed = True

    if not relays_failed and not mapping_failed:
        return jsonify({"success": True, "result": "success"})
    else:
        return jsonify({"result": "error", "relaysFailed": relays_failed, "mappingFailed": mapping_failed}), 500


@main_blueprint.route('/api/instance/oscilloscope', methods=['GET', 'POST'])
@requires_active
def oscilloscope_measurement():
    """
    Handles a potentiometer-related GET or POST.
    :param pot:
    :return:
    """
    if current_app.config['FAKE_HARDWARE']:
        from . import e2eresponses
        return jsonify(e2eresponses.STATE_RESPONSE_OSCILLOSCOPE_MEASUREMENT)

    instance_api = current_app.config['INSTRUMENTS_SERVER_API_URL']
    target_url = instance_api + "oscilloscope"

    print("[DBG]: Sending request to: " + target_url)

    if request.method == 'GET':
        # Just forward the request.
        r = requests.get(target_url, timeout=10)
        return (r.text, r.status_code, r.headers.items())

    elif request.method == 'POST':
        # Just forward the request.
        data = request.get_json(silent=True, force=True)
        r = requests.post(target_url, json=data, timeout=10)
        return (r.text, r.status_code, r.headers.items())


@main_blueprint.route('/api/instance/powersupply', methods=['POST'])
@requires_active
def powersupply_measurement():
    """
    Handles a potentiometer-related GET or POST.
    :param pot:
    :return:
    """
    if current_app.config['FAKE_HARDWARE']:
        from . import e2eresponses
        return jsonify(e2eresponses.STATE_RESPONSE_OSCILLOSCOPE_MEASUREMENT)

    instance_api = current_app.config['INSTRUMENTS_SERVER_API_URL']
    target_url = instance_api + "powersupply"

    print("[DBG]: Sending request to: " + target_url)

    # Just forward the request.
    data = request.get_json(silent=True, force=True)
    r = requests.post(target_url, json=data, timeout=10)
    return (r.text, r.status_code, r.headers.items())


@main_blueprint.route('/api/instance/functiongenerator', methods=['POST'])
@requires_active
def functiongenerator_measurement():
    """
    Handles a potentiometer-related GET or POST.
    :param pot:
    :return:
    """
    if current_app.config['FAKE_HARDWARE']:
        from . import e2eresponses
        return jsonify(e2eresponses.STATE_RESPONSE_OSCILLOSCOPE_MEASUREMENT)

    instance_api = current_app.config['INSTRUMENTS_SERVER_API_URL']
    target_url = instance_api + "functiongenerator"

    print("[DBG]: Sending request to: " + target_url)

    # Just forward the request.
    data = request.get_json(silent=True, force=True)
    r = requests.post(target_url, json=data, timeout=10)
    return (r.text, r.status_code, r.headers.items())


def reset_boards():
    """
    Helper func to send reset requests to the interface and instrument boards.
    The boards might actually be the same one, but reset is supposed to be idempotent anyway.
    :return:
    """
    logger.debug("Sending /reset requests")

    # Send reset events to the interface and device servers so we ensure that we start in a valid state.
    interface_server_api = current_app.config['INTERFACE_SERVER_API_URL']
    target_url = interface_server_api + "reset"
    r = requests.post(target_url, timeout=10)
    if r.status_code == 200:
        weblab_user.data['interface_server_online'] = True
    else:
        weblab_user.data['interface_server_online'] = False

    instruments_server_api = current_app.config['INSTRUMENTS_SERVER_API_URL']
    target_url = instruments_server_api + "reset"
    r = requests.post(target_url, timeout=10)
    if r.status_code == 200:
        weblab_user.data['instruments_server_online'] = True
    else:
        weblab_user.data['instruments_server_online'] = False

#######################################################
#
#   Other functions
#

def _check_csrf():
    expected = session.get('csrf')
    if not expected:
        current_app.logger.warning("No CSRF in session. Calling method before loading index?")
        return False

    obtained = request.values.get('csrf')
    if not obtained:
        # No CSRF passed.
        current_app.logger.warning("Missing CSRF in provided data")
        return False

    return expected == obtained
