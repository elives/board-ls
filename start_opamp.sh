#!/bin/bash

PORT=8551
WORKERS=1

_term() {
   kill -TERM "$child" 2>/dev/null
}

# When SIGTERM is sent, send it
trap _term SIGTERM


cd /home/elives/board-ls
. /home/elives/.virtualenvs/board-ls/bin/activate
. prodrc_opamp

date
export SCRIPT_NAME=/labs/opamp

gunicorn --pid opamp.gunicorn.pid --bind 127.0.0.1:$PORT -w $WORKERS --worker-class eventlet wsgi_app:application &

child=$!
wait "$child"
