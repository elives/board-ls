
# Board-LS 
E-Lives Reference Practical Work Laboratory Server.



## Using the tableread helper script to convert MS Word tables into relay tables
See the tableread.md document.


## Development

First, configure the board type. That can be done through the BOARD_TYPE variable that can be set in devrc.
The default is BOARD_TYPE=rlc

Run the server:
```
source devrc
flask run
```

Create a new session:
```
source devrc
flask weblab fake new --assigned-time 3600
```


## Partially automated deployment of Board-LS

Example for TTU:

```python3 run.py --ssh-config "/Users/lrg/Dropbox/LABSLAND/SSH_CONFIG/config.d/ttu" --host ttu --deploy board-ls --ssh-keys "/Users/lrg/Dropbox/LABSLAND/PROYECTOS/E-LIVES/TECHNICAL/KEYS/gen-deployment-key" --deployment ttu```
    
    
Example for UBMA:

```python3 run.py --ssh-config "/Users/lrg/Dropbox/LABSLAND/SSH_CONFIG/config.d/ubma" --host ubma --deploy board-ls --ssh-keys "/Users/lrg/Dropbox/LABSLAND/PROYECTOS/E-LIVES/TECHNICAL/KEYS/gen-deployment-key" --deployment ubma```

Example for UNIK:

```python3 run.py --ssh-config "/Users/lrg/Dropbox/LABSLAND/SSH_CONFIG/config.d/unik" --host ttu --deploy board-ls --ssh-keys "/Users/lrg/Dropbox/LABSLAND/PROYECTOS/E-LIVES/TECHNICAL/KEYS/gen-deployment-key" --deployment unik```

Example for Guelma:
```python3 run.py --ssh-config "/Users/lrg/Dropbox/LABSLAND/SSH_CONFIG/config.d/guelma" --host guelma --deploy board-ls --ssh-keys "/Users/lrg/Dropbox/LABSLAND/PROYECTOS/E-LIVES/TECHNICAL/KEYS/gen-deployment-key" --deployment guelma```