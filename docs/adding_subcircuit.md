
# Adding a subcircuit to a board

This Board-LS laboratory server is originally meant to support two practical works: an RLC board and an OpAmp board.
Each *board* provides different *subcircuits*. For example, the RLC board provides an "RC" subcircuit, a "LC" subcircuit,
etc. 

The laboratory server has been designed to be easy to modify and extend. One of the extensions you might want to add is a
new subcircuit to a particular board.

In this short guide, we will add a new subcircuit (in this case, a full RLC subcircuit) to the RLC board. 


## Starting point

At the moment of writing this, the RLC board supports five subcircuits:
  - Resistor-Resistor
  - Resistor-Capacitor
  - Capacitor-Resistor
  - Inductor-Resistor
  - Resistor-Inductor
  
We are going to add a new one, a Resistor-Inductor-Capacitor subcircuit. 

Note that all of those subcircuits are defined in the ```RLC - Tech Staff``` document by Abdelhalim.

![Resistor-Inductor-Capacitor spec](./images/rlc_rlc_subcircuit_spec.png "Resistor-Inductor-Capacitor spec")
Figure 1. Original specification for this subcircuit.


## Adding the diagram picture

All subcircuits have an associated diagram, which is basically shown in the background. *Change* buttons
and selected values will be placed over it.

In our case, the diagram we will use is the following:

![Resistor-Inductor-Capacitor diagram](./images/resistor_inductor_capacitor.png "Resistor-Inductor-Capacitor diagram")

Now that we have the image for our new subcircuit, we place it in the ```static/images/diagram/rlc/``` folder.
This is where the other RLC subcircuit diagram images can be found as well. We name it ```resistor_inductor_capacitor.png```.


## Adding the subcircuit to the selection dropdown

To do this, we simply need to register it into ```./templates/rlc/_dropdown.html```. 
We add the line:

```
<li><a href="#" data-board="rlc" data-subcircuit="resistor-inductor-capacitor">Resistor-Inductor-Capacitor circuit</a></li>
```


## Defining the subcircuit

Next step will be to actually define the subcircuit in the HTML. We will add a particular structure for the subcircuit.
This will reference the diagram image to use, the names of the subcircuit and components, and the actual values
that the components will be allowed to take.

We will take an existing subcircuit as an example: the resistor-inductor one.
The structure is the following:

```html
<!-- RLC resistor-inductor (Circuit 5) (Experiment 2D) -->
<div class="col-xs-12 col-sm-7 diagram-col rlc resistor-inductor" data-board="rlc"
     data-subcircuit="resistor-inductor" style="display: none">

    <!-- Background diagram -->
    <img src="{{ url_for('static', filename='images/diagrams/rlc/resistor_inductor.png') }}"/>

    <!-- Button to view function generator -->
    <button class="btn btn-success btn-md view-func-gen">View</button>

    <!-- Button to view the oscilloscope -->
    <button class="btn btn-success btn-md view-oscilloscope">View</button>

    <!-- R1: Change button and value. Though it is actually fixed. -->
    <div class="btn-group value-selector r1">
        <button class="btn btn-success btn-md change dropdown-toggle r1"
                data-toggle="dropdown">{{ gettext("Change") }}</button>

        <p class="val r1"></p>

        <ul class="dropdown-menu" role="menu">
            <li><a href="javascript:void(0)" data-value="r1">R1 &#x2126;</a></li>
            <li><a href="javascript:void(0)" data-value="r2" data-default>R2 &#x2126;</a></li>
            <li><a href="javascript:void(0)" data-value="r1ppr2">R1 || R2 &#x2126;</a></li>
        </ul>
    </div>

    <!-- L1: Change button and value. -->
    <div class="btn-group value-selector l1">
        <button class="btn btn-success btn-md change dropdown-toggle"
                data-toggle="dropdown">{{ gettext("Change") }}</button>

        <p class="val l1"></p>

        <ul class="dropdown-menu" role="menu">
            <li><a href="javascript:void(0)" data-value="l1">L1 H</a></li>
            <li><a href="javascript:void(0)" data-value="l2" data-default>L2 H</a></li>
            <li><a href="javascript:void(0)" data-value="l1ppl2">L1 || L2 H</a></li>
        </ul>
    </div>
</div>
```

The jQuery based system will rely on that structure to create the controls for choosing the values, and for extracting
the selected configuration. It is therefore critical to maintain that structure and to ensure the names are proper,
including the CSS class names.

The purpose of the header is mostly straightforward.
```html
<div class="col-xs-12 col-sm-7 diagram-col rlc resistor-inductor" data-board="rlc"
     data-subcircuit="resistor-inductor" style="display: none">
```

It references the main board (rlc), and the name of the subcircuit (resistor-inductor), in several places.


Also noteworthy are the value-selector blocks:
```html
<div class="btn-group value-selector r1">
    <button class="btn btn-success btn-md change dropdown-toggle r1"
            data-toggle="dropdown">{{ gettext("Change") }}</button>

    <p class="val r1"></p>

    <ul class="dropdown-menu" role="menu">
        <li><a href="javascript:void(0)" data-value="r1">R1 &#x2126;</a></li>
        <li><a href="javascript:void(0)" data-value="r2" data-default>R2 &#x2126;</a></li>
        <li><a href="javascript:void(0)" data-value="r1ppr2">R1 || R2 &#x2126;</a></li>
    </ul>
</div>
```

Those represent the options for a component. In this case, a resistor. The text values are important because they are
the ones shown. Also, the values inside the data-value attributes are important because they are the 'official' names
that will be used for defining the configuration, and they will later be expected to match.

With all that in mind, we add a new subcircuit block:

```html
<!-- RLC resistor-inductor-capacitor (Circuit 6) (Experiment 3) -->
<div class="col-xs-12 col-sm-7 diagram-col rlc resistor-inductor-capacitor" data-board="rlc"
     data-subcircuit="resistor-inductor-capacitor" style="display: none">

    <!-- Background diagram -->
    <img src="{{ url_for('static', filename='images/diagrams/rlc/resistor_inductor_capacitor.png') }}"/>

    <!-- Button to view function generator -->
    <button class="btn btn-success btn-md view-func-gen">View</button>

    <!-- Button to view the oscilloscope -->
    <button class="btn btn-success btn-md view-oscilloscope">View</button>

    <!-- L1: Change button and value. -->
    <div class="btn-group value-selector l1">
        <button class="btn btn-success btn-md change dropdown-toggle"
                data-toggle="dropdown">{{ gettext("Change") }}</button>

        <p class="val l1"></p>

        <ul class="dropdown-menu" role="menu">
            <li><a href="javascript:void(0)" data-value="l1">L1 H</a></li>
            <li><a href="javascript:void(0)" data-value="l2" data-default>L2 H</a></li>
            <li><a href="javascript:void(0)" data-value="l1ppl2">L1 || L2 H</a></li>
        </ul>
    </div>

    <!-- C1: Change button and value. -->
    <div class="btn-group value-selector c1">
        <button class="btn btn-success btn-md change dropdown-toggle"
                data-toggle="dropdown">{{ gettext("Change") }}</button>

        <p class="val c1"></p>

        <ul class="dropdown-menu" role="menu">
            <li><a href="javascript:void(0)" data-value="c1">C1 uF</a></li>
            <li><a href="javascript:void(0)" data-value="c2" data-default>C2 uF</a></li>
            <li><a href="javascript:void(0)" data-value="c1ppc2">C1 || C2 uF</a></li>
        </ul>
    </div>

    <!-- R1: Change button and value. Though it is actually fixed. -->
    <div class="btn-group value-selector r1">
        <button class="btn btn-success btn-md change dropdown-toggle r1"
                data-toggle="dropdown">{{ gettext("Change") }}</button>

        <p class="val r1"></p>

        <ul class="dropdown-menu" role="menu">
            <li><a href="javascript:void(0)" data-value="r3">R3 &#x2126;</a></li>
            <li><a href="javascript:void(0)" data-value="r4" data-default>R4 &#x2126;</a></li>
            <li><a href="javascript:void(0)" data-value="r3ppr4">R3 || R4 &#x2126;</a></li>
        </ul>
    </div>
</div>
```

The most noteworthy addition is probably that in this case we have three different value-selector blocks. Also,
we put them in order: L, C and R, to match the specification. That way the configuration for the relays will match.


## Placing the value-selectors in the diagram

If you tried the subcircuit now, you would notice that the value selectors are there but not placed apropriately within
the subcircuit diagram.

We also need to add a CSS block to ```static/styles/rlc.scss```. The blocks are mostly straightforward:
the classes must match those in the HTML block, and they mostly serve to position the value selectors properly.

We add the following:

```scss
// RLC resistor-inductor-capacitor
.diagram-col.rlc.resistor-inductor-capacitor {

    button.view-func-gen {
        position: absolute;
        top: -20px;
        left: 130px;
    }

    button.view-oscilloscope {
        position: absolute;
        top: -20px;
        left: 450px;
    }

    .btn-group.value-selector {
        &.r1 {
            top: 70%;
            left: 55%;
        }

        &.l1 {
            top: 65%;
            left: 20%;
        }

        &.c1 {
            top: 45%;
            left: 42%;
        }
    }
}
```

## Adding the relay table for the subcircuit

The subcircuits have a set of options, with a limited number of combinations. Each combination has a corresponding
set of relay states, through which that combination is implemented in the RLC hardware board. This forms a relay table,
which is specified in the specification word document, and which is transcribed to the ```views.py``` file, within the
CONFIG_TO_RELAYS dictionary.

To transcribe the table in the Word document into the CONFIG_TO_RELAYS dictionary, we can make use of the 
```helper/tableread.py```. Or we could also do it manually. 

Doing it through the script is faster. We select the values in the table, we copy-paste, and run the script.

![generating table](./images/generating_table.png "Generating relay table")
The figure above shows the script running to generate the table. Previously, the spec table in MS Word has been 
copied into the clipboard.

From here, we can add the generated relay table to the ```views.py``` file:

```python
    # RLC Resistor-Inductor-Capacitor (Circuit 6 aka Experiment 3)
    "rlc:resistor-inductor-capacitor:l1_c1_r3": [1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0],
    "rlc:resistor-inductor-capacitor:l1_c1_r4": [1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1],
    "rlc:resistor-inductor-capacitor:l1_c1_r3ppr4": [1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 1],
    "rlc:resistor-inductor-capacitor:l1_c2_r3": [1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0],
    "rlc:resistor-inductor-capacitor:l1_c2_r4": [1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1],
    "rlc:resistor-inductor-capacitor:l1_c2_r3ppr4": [1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 1],
    "rlc:resistor-inductor-capacitor:l1_c1ppc2_r3": [1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 1, 0],
    "rlc:resistor-inductor-capacitor:l1_c1ppc2_r4": [1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1],
    "rlc:resistor-inductor-capacitor:l1_c1ppc2_r3ppr4": [1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 1, 1],
    "rlc:resistor-inductor-capacitor:l2_c1_r3": [1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0],
    "rlc:resistor-inductor-capacitor:l2_c1_r4": [1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1],
    "rlc:resistor-inductor-capacitor:l2_c1_r3ppr4": [1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 1],
    "rlc:resistor-inductor-capacitor:l2_c2_r3": [1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0],
    "rlc:resistor-inductor-capacitor:l2_c2_r4": [1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1],
    "rlc:resistor-inductor-capacitor:l2_c2_r3ppr4": [1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 1],
    "rlc:resistor-inductor-capacitor:l2_c1ppc2_r3": [1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0],
    "rlc:resistor-inductor-capacitor:l2_c1ppc2_r4": [1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1],
    "rlc:resistor-inductor-capacitor:l2_c1ppc2_r3ppr4": [1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 1],
    "rlc:resistor-inductor-capacitor:l1ppl2_c1_r3": [1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 1, 0],
    "rlc:resistor-inductor-capacitor:l1ppl2_c1_r4": [1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1],
    "rlc:resistor-inductor-capacitor:l1ppl2_c1_r3ppr4": [1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 1, 1],
    "rlc:resistor-inductor-capacitor:l1ppl2_c2_r3": [1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0],
    "rlc:resistor-inductor-capacitor:l1ppl2_c2_r4": [1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1],
    "rlc:resistor-inductor-capacitor:l1ppl2_c2_r3ppr4": [1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 1],
    "rlc:resistor-inductor-capacitor:l1ppl2_c1ppc2_r3": [1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 1, 0],
    "rlc:resistor-inductor-capacitor:l1ppl2_c1ppc2_r4": [1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1],
    "rlc:resistor-inductor-capacitor:l1ppl2_c1ppc2_r3ppr4": [1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0, 0, 1, 1],
```

Some of the names for the configuration may actually need to be fixed by hand.


## Conclusion

After all these steps, the subcircuit should be ready. It should appear in the upper dropdown. When selected, it should 
load the circuit diagram, along with the proper value selectors. The value selectors should have the potential values.
When the potential values are switched, a configuration request will be sent to the Interface Server. The request
will retrieve the relay configuration from the config table.


## Authors

The original version of this guide has been writen by Luis Rodriguez-Gil (luis@labsland.com),
from LabsLand (https://labsland.com), as part of the e-Lives project.