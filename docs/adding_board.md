
# Adding a board

The *board-ls* laboratory is originally meant to support two
different hardware boards: an RLC circuit and an OpAmp circuit.

It is, however, designed to be easy to adapt and extend to new boards.

In this short guide, we will describe how to add a new board, 
which we will call *filter*. Note that this board is not one of
the two reference boards, and thus it might actually not be
incorporated into the repository or fully implemented.

## Starting point

The laboratory supports two boards:
  - RLC (with the internal identifier 'rlc')
  - OpAmp (with the internal identifier 'opamp')
  
 We are going to add a new Filter board (with the internal identifier 'filter')
 
## Creating diagrams directory

The new board should have its own directory for the subcircuit
diagrams. For this, we create a *filter* folder into the
````static/images/diagrams```` directory.


## Creating templates directory

We need to create the template directories for the new board.
The new directory should be called *filter* and be within the
````templates/```` directory. The easiest way to create it
would be to copy one of the existing directories, for the
OpAmp or the RLC.

The new *filter* directory should have the following files inside:
- _diagrams.html
- _dropdown.html

The ````_diagrams.html```` file contains the subcircuits for the
board, while the `````dropdown.html````` file contains the list
of subcircuits as will be shown in the subcircuit dropdown
in the top of the screen. For more information on the contents
of these files, see the [guide on adding a subcircuit](./adding_subcircuit.md).


## Registering the board in the views file

The *start* function in the ````views.py```` file references the existing boards.
This is important, among other reasons, because the board shown will actually
depend on the laboratory configuration.

The following lines at the end of the *start* function reference the
*opamp* and *rlc* boards:

````python
    # Initialize the board.
    board = current_app.config['BOARD_TYPE']
    if board not in ['opamp', 'rlc']:
        raise Exception('Unsupported type of board: {}'.format(board))

    # Initialize the subcircuit.
    subcircuit = weblab_user.data.get('subcircuit')
    if subcircuit is None:
        if board == 'rlc':
            subcircuit = 'resistor-resistor'
        elif board == 'opamp':
            subcircuit = 'inverting-amplifier'
    weblab_user.data['subcircuit'] = subcircuit
````

We add support for our new *filter* board:

    
````python
    # Initialize the board.
    board = current_app.config['BOARD_TYPE']
    if board not in ['opamp', 'rlc', 'filter']:
        raise Exception('Unsupported type of board: {}'.format(board))

    # Initialize the subcircuit.
    subcircuit = weblab_user.data.get('subcircuit')
    if subcircuit is None:
        if board == 'rlc':
            subcircuit = 'resistor-resistor'
        elif board == 'opamp':
            subcircuit = 'inverting-amplifier'
        elif board == 'filter':
            subcircuit = 'basic-filter'
    weblab_user.data['subcircuit'] = subcircuit
````

Please note that the ````basic-filter```` subcircuit does not actually
exist. Should you want to add such a circuit, you would need to follow the
[guide on adding a subcircuit](./adding_subcircuit.md).
    


## Conclusion

After following the steps above, you should now have a working *filter* board, 
provided you have also added some subcircuits for it.

With this scheme it should be possible to support any kind of
similar remote laboratory, which features:
  - Circuits controllable through relays.
  - Various instruments such as oscilloscopes etc.
  - Subcircuits with electrical components that can be chosen
    among a list of potential options.
    
    
## Authors

The original version of this guide has been writen by Luis Rodriguez-Gil (luis@labsland.com),
from LabsLand (https://labsland.com), as part of the e-Lives project.