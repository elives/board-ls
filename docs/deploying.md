
# Deploying

This Board-LS laboratory server is originally meant to support two practical works: an RLC board and an OpAmp board.
Each *board* provides different *subcircuits*. For example, the RLC board provides an "RC" subcircuit, a "LC" subcircuit,
etc. 

This short guide describes how to deploy the Board-LS, and specifically how to deploy two instances:
one or the RLC and one for the OpAmp.

## Assumptions

This guide is designed for the e-Lives project. It assumes that the Board-LS servers are to be deployed
in the main server, and that that main server is running a relatively clean and recent installation of 
Ubuntu server.

For the purposes of this guide, we will also assume that the server is also the one running WebLab-Deusto.

The guide may also assume basic knowledge of:
- Using the Linux shell.
- The [Git source control system](https://git-scm.com/book/en/v2/Getting-Started-About-Version-Control).
- The [Apache HTTP server](https://httpd.apache.org/).
- Deploying (not programming) [Python](https://www.python.org)-based servers.


## Requirements

You will need to install certain dependencies through *apt*. 

```
sudo apt install redis-server python3 python3-virtualenv virtualenvwrapper supervisor git vim
```

You will also need to have a working version of Node. Ubuntu repositories do have it available, but I find that its
distribution tends to lead to certain issues and inconsistencies. If you are used to working with the Node distribution
of Ubuntu you can install it through *apt*. If you are not, then I recommend that you install nvm first (a node package manager):

```
wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.3/install.sh | bash
```

Now you will need to restart your terminal or reload your terminal's bashrc equivalent.

Then, you can install a node:

```
nvm install stable
```

And activate it:

```
nvm use stable
```

I recommend that before following to the next step, you ensure that your Node installation is working properly.

For that, if you type: ```node```, the Node javascript interpreter should open. And if you type: ```npm```, the
npm tool should be working.
 

## Preparing and cloning

First, create an ``elives`` user in your Linux:

````
sudo adduser elives
````

You should deploy everything on it.

Change to that user and go to the home folder.

```
sudo su - elives
cd ~
```

Now you will need to clone the repository. To do so, you will first need to ensure that you have
access. Eventually it will be open, as of now, you can contact luis@labsland.com to receive an invitation.
We are using [GitLab](https://gitlab.com) as a host.

When you are ready, clone it:

```
git clone git@gitlab.com:elives/board-ls
```

If it does not succeed, please ensure that you have rights to the repository.

Now, setup a virtualenv to use with the project:

```
mkvirtualenv board-ls --python /usr/bin/python3
```

It should be Python 3 (Python 2 is not supported).

Enter into the virtualenv: 
```
workon board-ls
```

Note that you will need to repeat this step (entering into the virtualenv) whenever you want to run the project.


Now install the Python requirements for the project:

```
pip install -r requirements.txt
```

And you will also need to install the web (npm) requirements for it:
```
cd mylab
npm install
```

This will install several Javascript libraries for Node.


## Board-LS configuration

If you completed the previous step successfully, then you have every dependency and are almost ready to run 
the project. However, there are some details that are dependant upon your particular environment. Those
configuration settings are defined in the ```config.py``` file but are meant to be changed through
environment variables. 

Particularly, the recommended approach is to create a prodrc file that contains the variables.

The following is an example *prodrc* file. 

You should create two files: They should be called prodrc_opamp and prodrc_rlc respectively.
They should be located within the board-ls folder. 

The contents should be similar to the following, but you should customize the variables to your particular
environment. The example that follows is for the RLC, but for the OpAmp one, it should be similar.

```
export ENVIRONMENT=production
export FLASK_APP=autoapp.py
export INTERFACE_SERVER_API_URL='http://192.168.10.20:5000/instances/1/'

export SECRET_KEY='put-anythng-random-here-and-secret'
export FLASK_CONFIG='production'
export SCRIPT_NAME='/labs/rlc'
export SESSION_COOKIE_PATH='/labs/rlc'

export CAMERA_URL='https://weblab.unifesp.br/cams/cams/usbcam1'

export WEBLAB_USERNAME=weblabdeusto
export WEBLAB_PASSWORD=should-match-what-is-configured-in-weblab-deployment-config

export REDIS_BASE=rlc

export BOARD_TYPE=rlc

export INSTITUTION=mostaganem

. /home/elives/.virtualenvs/board-ls/bin/activate
```

Most of the variables should be self-evident. Though I will describe some of them just in case.

The INTERFACE_SERVER_API_URL is the address of the server in the Raspberry Pi device (the device server).
So in the case of the RLC, it should be the IP to the Raspberry that controls the RLC board.
Be careful with the path. ````instances/1/```` must be kept as proposed. The internal device server
expects that path to exist, because though we do not use them, it supports different instances on the same
server.

The SCRIPT_NAME and SESSION_COOKIE_PATH are particularly critical. If they are not right and they do
not match what you will later configure in Apache, then your server will not work. Normally you won't
need to change this. Though you will need to use labs/opamp for the OpAmp instead.

The CAMERA_URL should point to an endpoint that returns the current frame for your camera. The camera that 
is pointing at e.g. the RLC board. In some servers we are installing our own webcam system (contact luis@labsland.com for
more information on this), though you can install any webcam system you want, or even serve a camera image directly.

It is critical, however, that the URL that you provide here is publicaly accessible from the Internet. It should
NOT be behind any firewall. Third parties should be able to access that particular URL.

BOARD_TYPE should match an existing BOARD_TYPE, normally rlc or opamp. If you implement your own, you would make it
match here. You can see the guide on deploying new boards or contact us, but it is beyond the scope of this 
particular guide.


## Auto-start configuration

The server should autostart automatically, and be deployed as a service. 
We recommend ````supervisor```` for auto-starting the server. Though describing how to use supervisor is beyond the
scope of this guide, you can find other guides on the Internet easily if needed.

We provide some example supervisor scripts in the ````supervisor```` folder of the board-ls project.
There are two: one for running the a web server, the other a background worker. Both are necessary, and you need
to have copies of each for each of the boards that you are running.

That is, one set for the RLC and one for the OpAmp.

You can copy them into ```/etc/supervisor/conf.d```, give them appropriate names, and ensure the configuration
inside matches your setup.

In supervisor, once you have added the files and configured them appropriately, you can do:

```supervisor update```

To ensure that they are loaded. They will also be run then.

You can run: 
```sudo supervisor status```

To check if they are effectively running (if they are not, you will find logs in the project folder, which will
let you debug the potential issues.

For the future, you can do:
```
sudo supervisor stop all
```
To stop all processes running under supervisor, or
```
sudo supervisor start all
```
To start them all.


We suggest that before proceeding to the next step you verify that under supervisor, you have at least four
components running successfully:
- The OpAmp worker.
- The OpAmp web server.
- The RLC worker.
- The RLC web server.


## Apache configuration

Although you could use a different web server, we propose that you use and configure Apache.

The goal is that the Board-LS servers that you are running locally (they listen to localhost in a port such as e.g. 5000) be 
served "publicly".

You can find an example of an Apache configuration file in the ````deployments/example/labs.conf```` file.

If you simply copy the file into the conf-available folder of Apache, and enable it; if you have all the required
Apache modules enabled, it might work as-is. 


## WebLab-Deusto configuration

Finally, both laboratories should be configured and added to the WebLab-Deusto instance that is probably 
running on the same computer. 

The process to do this does take some effort. 

A guide already exists and is present here: 
https://weblabdeusto.readthedocs.io/en/latest/remote_lab_deployment.html

If you come across issues, you can also contact us at luis@labsland.com or pablo@labsland.com for help.


## Conclusions

After completing this guide, you should end up with two working board-ls servers, one for the OpAmp and one for the RLC.
You should be able to access them through your WebLab-Deusto instance.

They won't necessarily work yet, of course, because they also require the "board-is": the flask-based device server
that is to be installed in the Raspberry PIs. The process of installing the board-is server is (or should be)
significantly easier than this one, and you can read about it on its own guide.

Note that though you or your students may indeed be able to access the labs through WebLab-Deusto (and through Gateway4Labs if you
install it as well), you might also want to access them through the LabsLand platform. While some limitations might exist
(LabsLand has somewhat strict requirements on public labs) we can generally integrate the lab here as well, for project members, which has
many practical advantages. Contact us for this (contact@labsland.com).








